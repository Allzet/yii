<?php

namespace mbischof\blog;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'mbischof\blog\controllers';

    public function init()
    {
        parent::init();
    }
}

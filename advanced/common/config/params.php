<?php

//basepathloc= php_sapi_name() == 'cli'? "php/os/OpenServer/domains/yii-ad.local/advanced/backend/web/resources/images/news" : "/resources/images/news";
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'fileManager' => [
        'documentroot' => realpath($_SERVER['DOCUMENT_ROOT'] . '/../../backend/web'),
        'adapter' => "file",
        'news' => [
            'basepath' => "/resources/images/news",
            'pattern' => "[date('Y/m/d', strtotime('{created}'))]",
            'width' => 630,
            'height' => 287,
            'miniatures' => [
                'display' => [
                    'width' => 630,
                    'height' => 287,
                    'crop_resize' => TRUE,
                ],
                'front' => [
                    'width' => 270,
                    'height' => 123,
                    'crop_resize' => TRUE,
                ],
                'thumb' => [
                    'width' => 203,
                    'height' => 140,
                    'crop_resize' => TRUE,
                ],
                'preview' => [
                    'width' => 102,
                    'height' => 70,
                    'crop_resize' => TRUE,
                ],
            ],
        ],
        'newspars' => [
            'basepath' => "php/os/OpenServer/domains/yii-ad.local/advanced/backend/web/resources/images/news",
            'pattern' => "[date('Y/m/d', strtotime('{created}'))]",
            'width' => 630,
            'height' => 287,
            'miniatures' => [
                'display' => [
                    'width' => 630,
                    'height' => 287,
                    'crop_resize' => TRUE,
                ],
                'front' => [
                    'width' => 270,
                    'height' => 123,
                    'crop_resize' => TRUE,
                ],
                'thumb' => [
                    'width' => 203,
                    'height' => 140,
                    'crop_resize' => TRUE,
                ],
                'preview' => [
                    'width' => 102,
                    'height' => 70,
                    'crop_resize' => TRUE,
                ],
            ],
        ],
        'photogalery' => [
            'basepath' => "resources/images/photogalery",
            'pattern' => "[date('Y/m/d', strtotime('{created}'))]",
            'width' => 700,
            'height' => 467,
            'miniatures' => [
                'display' => [
                    'width' => 700,
                    'height' => 467,
                    'crop_resize' => false,
                ],
                'preview' => [
                    'width' => 100,
                    'height' => 67,
                    'crop_resize' => false,
                ],
            ],
        ],
        'user' => [
            'basepath' => "resources/images/user",
            'pattern' => "[date('Y/m/d', strtotime('{created}'))]",
            'width' => 600,
            'height' => 600,
            'miniatures' => [
                'display' => [
                    'width' => 200,
                    'height' => 200,
                    'crop_resize' => true,
                ],
                'preview' => [
                    'width' => 100,
                    'height' => 100,
                    'crop_resize' => true,
                ],
            ],
        ],
    ],
];

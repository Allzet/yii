<?php

namespace common\helpers;

use Yii;
use file\Manager\FileFactory;
use file\Manager;

class Image
{
    public static $link;

    public static function image($options, $filename = NULL, $miniature = NULL, $data = array(), $dummy = TRUE)
    {

        $asset = NULL;

        if (is_array($options)) {
            if (array_key_exists('asset', $options)) {
                $asset = $options['asset'];
            }
            if (array_key_exists('filename', $options)) {
                $filename = $options['filename'];
            }
            if (array_key_exists('miniature', $options)) {
                $miniature = $options['miniature'];
            }
            if (array_key_exists('data', $options)) {
                $data = $options['data'];
            }
            if (array_key_exists('dummy', $options)) {
                $dummy = (boolean)$options['dummy'];
            } else {
                $dummy = TRUE;
            }
        } else {
            $asset = $options;
        }

        if (!is_array($data)) {
            $data = array('id' => $data);
        }
        Yii::setAlias('@file', dirname(dirname(__DIR__)) . '/backend/modules/file');
        $options = Yii::$app->params;

        /* @var  \file\Manager\Adapter\File $fileManager */
        $fileManager = FileFactory::factory((object)$options['fileManager']);

        $fileManager->setAsset($asset, $data);

        if (empty($filename)) {
            if ($dummy) {
                return $fileManager->getDummy($miniature);
            } else {
                return;
            }
        }

        $destination = $fileManager->getDestination() . '/' . $filename;

        if ($fileManager->isImage($destination, true)) {

            return $fileManager->getPath($filename, $miniature);
        }

        self::$link = $fileManager->getPath($filename);

        return false;
    }

    public static function getLink() {

        return self::$link;
    }

    /**
     * Return pixel to output
     */
    public static function makePixel()
    {
        header('Content-Type:image/gif');
        echo base64_decode('R0lGODlhAQABAJAAAP8AAAAAACH5BAUQAAAALAAAAAABAAEAAAICBAEAOw==');
        exit();
    }
}

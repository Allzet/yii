<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\BlogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Blogs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Blog', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_user',
            'name:ntext',
            //'text:ntext',
            'status'=> [
                'attribute' => 'status',
                'filter' => [
                    \frontend\models\Blog::IS_ACTIVE => 'активна',
                    \frontend\models\Blog::IS_DEACTIVE => 'неактивна',
                ],
                'value' => function ($model) {

            $status = [
                \frontend\models\Blog::IS_ACTIVE => 'активна',
                \frontend\models\Blog::IS_DEACTIVE => 'неактивна',
            ];

            return isset($status[$model['status']]) ? $status[$model['status']] : '';
        },
                'options' => [
                    'style' => 'width : 140px; text-align: center;',
                ],
            ],
            'datecreate:date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>

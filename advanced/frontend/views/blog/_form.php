<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use backend\widgets\CKEditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model frontend\models\Blog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="blog-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php
    if (empty($model->id_user)) {
        echo $form->field($model, 'id_user', ['inputOptions' => [ 'value' => Yii::$app->user->identity->id]])->hiddenInput();
        echo Yii::$app->user->identity->username . '<br><br>';
       
    }else{
        echo $form->field($model, 'id_user', ['inputOptions' => [ 'value' => $model->id_user]])->hiddenInput();
        $us = \backend\models\User::findOne($model->id_user);
        echo $us->username . '<br><br>';
    }

    ?>

    

    <?= $form->field($model, 'name')->textInput() ?>

    <?php //echo $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?php
     echo $form->field($model, 'text')->widget(CKEditor::className(), [
                            'clientOptions' => [
                                'toolbarGroups'             => [
                                    ['name' => 'clipboard', 'groups' => ['mode', 'undo', 'selection', 'clipboard', 'doctools']],
                                    ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
//                '/',
                                    ['name' => 'paragraph', 'groups' => ['templates', 'list', 'indent', 'align']],
                                    ['name' => 'insert'],
//                '/',
                                    ['name' => 'editing', 'groups' => ['tools', 'about']],
                                    ['name' => 'links'],
                                    ['name' => 'others'],
                                    ['name' => 'colors'],
                                ],
                                'filebrowserBrowseUrl'      => '/ckeditor/kcfinder/browse.php?type=files',
                                'filebrowserImageBrowseUrl' => '/ckeditor/kcfinder/browse.php?type=images',
                                'filebrowserFlashBrowseUrl' => '/ckeditor/kcfinder/browse.php?type=flash',
                                'filebrowserUploadUrl'      => '/ckeditor/kcfinder/upload.php?type=files',
                                'filebrowserImageUploadUrl' => '/ckeditor/kcfinder/upload.php?type=images',
                                'filebrowserFlashUploadUrl' => '/ckeditor/kcfinder/upload.php?type=flash',
                            ],
                            'preset'        => 'custom',
                        ]);
    
    
    
    
    echo $form->field($model, 'status')->widget(SwitchInput::classname(), [
        'pluginOptions' => [
            'handleWidth' => 80,
            'onText' => 'Активна',
            'offText' => 'Неактивна']
    ]); 
    //echo $form->field($model, 'status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Створити' : 'Зберегти', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

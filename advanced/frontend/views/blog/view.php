<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\widgets\AllzetView\AllzetView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Blog */

$params = \backend\models\User::findOne($model->id_user);
//        echo '<pre>';
//        print_r($params);
//        exit;

$this->title = '(' . $params->name . ' ' . $params->surname . ') ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Блоги', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-view">

    <?php
    if ($model->id_user == Yii::$app->user->identity->id or Yii::$app->user->identity->role > 5) {
        ?>

        <p>
            <?= Html::a('Редагувати', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?=
            Html::a('Видалити', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Видалити з кінцями?',
                    'method' => 'post',
                ],
            ])
            ?>
        </p>

        <?php
    }
    ?>
    <table style="text-align: left; width: 100%;" border="0"
           cellpadding="2" cellspacing="2">
        <tbody>
            <tr>
                <td colspan="1" rowspan="3" style="text-align: left; vertical-align: top; width: 300px;">

                    <?php echo '  Дата та час: ' ?><?= AllzetView::widget(['model' => $model, 'attributes' => ['datecreate',],]) ?>
                    <?php
                    echo
                    AllzetView::widget(['model' => $params, 'attributes' => [[
                        'label' => 'Зображення',
                        'value' => '<img src="' . \common\helpers\Image::image('user', unserialize($params->photo), 'display', ['created' => $params->datecreate]) . '">',
                        'format' => 'html',
                            ],],]);
                    ?>
                    <?php
                    if ($model->id_user == Yii::$app->user->identity->id or Yii::$app->user->identity->role > 5) {

                        echo 'Ідентифікатор посту: ' . AllzetView::widget(['model' => $model, 'attributes' => ['id',],]);
                        if ($model->status ? $x='Активна' : $x='Неактивна');  
                        echo 'Стаус: ' . 
                                AllzetView::widget(['model' => $model, 'attributes' => [[
                        'label' => 'Статус',
                        'value' => $x,
                        'format' => 'html',
                            ],],]);
                                
                    }
                    ?>



                </td>
                <th style=" width: 900px; vertical-align: top;">
        <h3>
            <?php
            echo
            AllzetView::widget(['model' => $params, 'attributes' => [[
                'label' => "Ім'я",
                'value' => '' . $params->name . ' ' . $params->surname . ' (Блогер)',
                'format' => 'html',
                    ],],]);
            ?>
        </h3>
        </th>
        </tr>
        <tr>

            <th style="width: 900px; text-align: center; vertical-align: top;">
        <h2>
<?php echo AllzetView::widget(['model' => $model, 'attributes' => ['name:ntext',],]) ?>
        </h2>
        </th>
        </tr>
        <tr>
            <td style="width: 900px; vertical-align: top;">
<?= AllzetView::widget(['model' => $model, 'attributes' => ['text:raw',],]) ?>

            </td>
        </tr>

        </tbody>
    </table>
</div>

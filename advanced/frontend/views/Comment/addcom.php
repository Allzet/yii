<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Comment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    //if (empty(frontend\models\News::$id_new)) {

    if (empty(\backend\models\Comment::$id_new)) {

        echo $form->field($model, 'id_news')->textInput();
    } else {
        echo $form->field($model, 'id_news', ['inputOptions' => [ 'value' => \backend\models\Comment::$id_new]])->hiddenInput();
        echo \backend\models\Comment::$id_new . '<br><br>';
    }
    echo $form->field($model, 'id_last', ['inputOptions' => [ 'value' => backend\models\Comment::$id_coment]])->hiddenInput();
    echo $x . '<br><br>';

    if (empty(Yii::$app->user->identity->username)) {
        echo $form->field($model, 'username')->textInput(['maxlength' => 255]);
    } else {
        echo $form->field($model, 'username', ['inputOptions' => [ 'value' => Yii::$app->user->identity->username]])->hiddenInput();
        echo '<strong>' . Yii::$app->user->identity->username . '</strong>';
        echo '<br>';
        echo '<br>';
    }
    if (empty(Yii::$app->user->identity->email)) {
        echo $form->field($model, 'email')->textInput();
    } else {
        echo $form->field($model, 'email', ['inputOptions' => [ 'value' => Yii::$app->user->identity->email]])->hiddenInput();
        echo '<strong>' . Yii::$app->user->identity->email . '</strong>';
        echo '<br>';
        echo '<br>';
    }
    echo $form->field($model, 'status', ['inputOptions' => [ 'value' => 1]])->hiddenInput();
    echo $form->field($model, 'text')->textarea(['rows' => 6]);
    $model->mod = 0;
    ?>
    <div class="form-group">
        <?= Html::submitButton('Додати коментар', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

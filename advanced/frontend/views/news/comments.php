<?php

use yii\widgets\ListView;

echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemOptions' => ['class' => 'item'],
    'itemView' => function ($model, $key, $index, $widget) {
return Yii::$app->controller->renderPartial('comment-item', ['model' => $model]);

},
]);
?>


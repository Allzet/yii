<?php

use backend\widgets\AllzetView\AllzetView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\models\News */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Всі новини', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">

    <!--<h1><?php echo Html::encode($this->title) ?></h1>-->

    <table style="text-align: left; width: 100%;" border="0"
           cellpadding="2" cellspacing="2">
        <tbody>
            <tr>
                <td colspan="1" rowspan="3" style="text-align: left; vertical-align: top; width: 300px;">

                    <?php
                    echo '  Дата та час: ' . AllzetView::widget(['model' => $model, 'attributes' => ['putdate',],]);
                    echo AllzetView::widget(['model' => $model, 'attributes' => [[
                        'label' => 'Зображення',
                        'value' => '<img src="' . \common\helpers\Image::image('news', unserialize($model->url_pict)[0], 'thumb', ['created' => $model->crdate]) . '">',
                        'format' => 'html',
                            ],],]);
                    echo 'Рубрика: ' . AllzetView::widget(['model' => $model, 'attributes' => ['id_category',],]);
                    echo 'Посилання (текст): ' . AllzetView::widget(['model' => $model, 'attributes' => ['url_text:ntext',],]);
                    echo 'Посилання: ' . AllzetView::widget(['model' => $model, 'attributes' => ['url:ntext',],]);

                    $gallery = \backend\models\Photogalery::findOne($model->photo);
                    ?>
                </td>
                <th style=" width: <?php echo $com_width ?>px; text-align: center; vertical-align: top;">
        <h1>
            <?php echo AllzetView::widget(['model' => $model, 'attributes' => ['name:ntext',],]) ?>
        </h1>
        </th>
        </tr>
        <tr>
            <th style="width: <?php echo $com_width ?>px; vertical-align: top;">
                <?php echo AllzetView::widget(['model' => $model, 'attributes' => ['descript:ntext',],]) ?>
            </th>
        </tr>
        <tr>
            <td style="width: <?php echo $com_width ?>px; vertical-align: top;">
                <?php
                echo AllzetView::widget(['model' => $model, 'attributes' => ['body:raw',],]);

                if (!empty($gallery['arr_pictures'])) {

                    $items = json_decode($gallery['arr_pictures']);
                    $itemdesc = json_decode($gallery['desc_pictures']);
                    $widget = backend\widgets\fotorama\Widget::begin([
                                'version' => '4.5.2',
                                'options' => [
                                    'nav' => 'thumbs',
                                ],
                                'htmlOptions' => [
                                    'class' => 'fotorama',
                                    'data-navposition' => 'top',
                                ],
                    ]);

                    foreach ($items as $k => &$v) {
                        $v = ['<img src="' . \common\helpers\Image::image('photogalery', $v, 'display', ['created' => $gallery['date_create']]) . '"' .
                            'data-caption="' . $itemdesc[$k] . '"' .
                            '>'];
                    }
                    print_r($items);
                    $widget->end();
                }
                ?>
            </td>
        </tr>
        </tbody>
    </table>
    <br><br><br>
    <?php
    \frontend\models\News::$id_new = $model['id_news'];
    \frontend\models\News::$id_coment = '0';
    $countcom = \frontend\models\CommentSearch::commentNumberToNews($model->id_news);
    $com_width = \frontend\models\News::$com_width;
    ?>
    <table width="<?php echo $com_width - 15; ?>" border="0" bgcolor="#fbfbfb" style="border-bottom:1px solid #999999;"  cellpadding="2" cellspacing="2">
        <tr>
            <td>
<?php
if (!empty($countcom)) {
    echo 'Коментарів: ' . $countcom;
} else {
    echo 'Коментарів нема';
}
?>
            </td>
            <td align="right">
                <a name="nach" id="ncom"></a> <a style="cursor:pointer;" onClick="showlayer('komm')">Прокоментувати</a>
            </td>
        </tr>
        <tr>
            <td  colspan="2" rowspan="1">
<?php
echo '<div id="komm" style="display:none; padding:10px;  width:' . $com_width . '-15 ;" align="left">';
echo $this->render('//comment/create', ['model' => new \frontend\models\Comment()]);
echo '</div>';

Pjax::begin([
    'id' => 'comments'
]);
echo $comments;
Pjax::end();
?>             
            </td>
        </tr>
    </table>
</div>



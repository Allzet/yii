<?php
use yii\helpers\Html;
use backend\widgets\AllzetView\AllzetView;
use yii\widgets\DetailView;
use yii\imagine\Image;
?>

<table style="text-align: left; width: 100%;" border="0"
 cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td
          style="text-align: left; vertical-align: top; width: 100px;">
     <?php echo $model  ['putdate']; ?>
      </td>
      <td
          style="width: 100px; text-align: left; vertical-align: top;">
     <?php echo $model  ['id_category']; ?>
      </td>
      <td
 style="width: 380px; text-align: left; vertical-align: top;"
          colspan="1" rowspan="2">
     <?php
        echo Html::a(Html ::encode($model  ['name']), ['view', 'id' => $model ['id_news']]);
        ?>
        <div>
            <?php echo $model  ['descript']; ?>
        </div>
      </td>
    </tr>
    <tr>
      <td
 style="width: 200px; text-align: left; vertical-align: top;"
          colspan="2" rowspan="1">
     <?=
                     AllzetView::widget(['model' => $model, 'attributes' => [[
                        'label' => 'Зображення',
                        'value' => '<img src="' . \common\helpers\Image::image('news', unserialize($model->url_pict)[0], 'thumb', ['created' => $model->crdate]) . '">',
                        'format' => 'html',
                            ],],])
                    ?>
      </td>
    </tr>
    <tr>
      <td colspan="3" rowspan="1">----------------------</td>
    </tr>
  </tbody>
</table>



<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use backend\widgets\AllzetView\AllzetView;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Новини ТЕСТ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    
    
    
    
    <h1><?= Html::encode($this->title) ?></h1>
    <?php /// echo $this->render('_search', ['model' => $searchModel]); ?>
<table style="text-align: left; width: 100%;" border="0"
 cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td></td>
      <td colspan="3" rowspan="1"></td>
      <td></td>
    </tr>
    <tr>
      <td
          style="width: 300px; text-align: left; vertical-align: top;">
     <ul>
        <?=
        ListView::widget([
            'dataProvider' => $dataProvider,
            'itemOptions' => ['class' => 'item'],
            'itemView' => function ($model, $key, $index, $widget) {
        return $this->render('news-item', ['model' => $model]);
    },
        ]);
        ?>
    </ul>
      </td>
      <td
 style="width: 600px; text-align: left; vertical-align: top;"
          colspan="3" rowspan="1">
     <ul>
        <?=
        ListView::widget([
            'dataProvider' => $dataProvider,
            'itemOptions' => ['class' => 'item'],
            'itemView' => function ($model, $key, $index, $widget) {
        return $this->render('newstop', ['model' => $model]);
    },
        ]);
        ?>
    </ul>
      </td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>

   
</div>

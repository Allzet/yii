<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id_com
 * @property integer $id_news
 * @property integer $id_last
 * @property string $date_cr
 * @property string $date_ed
 * @property integer $user
 * @property integer $ident
 * @property string $email
 * @property string $text
 * @property integer $mod
 *
 * @property News $idNews
 * @property User $user0
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_news', 'id_last', 'username', 'email', 'text'], 'required'],
            [['id_news', 'id_last',  ], 'integer'],
            [['date_cr', ], 'safe'],
            [['email', 'text', 'username'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_com' => 'Id Com',
            'id_news' => 'Id News',
            'id_last' => 'Id Last',
            'username' => 'Username',
            'email' => 'Email',
            'text' => 'Text',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdNews()
    {
        return $this->hasOne(News::className(), ['id_news' => 'id_news']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser0()
    {
        return $this->hasOne(User::className(), ['username' => 'username']);
    }
}

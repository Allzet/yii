<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "News".
 *
 * @property integer $id_news
 * @property string $name
 * @property string $descript
 * @property string $body
 * @property string $crdate
 * @property string $putdate
 * @property string $url
 * @property string $url_text
 * @property string $url_pict
 * @property integer $id_category
 * @property integer $hide
 * @property integer $top
 * @property integer $blood
 * @property integer $photo
 */
class News extends \yii\db\ActiveRecord
{
    const IS_HIDE = 1;
    const IS_SHOW = 0;
    const IS_TOP = 1;
    const IS_NOTOP = 0;
    const IS_BLOOD = 1;
    const IS_NOBLOOD = 0;
    public static $id_new;
    public static $id_coment;
    public static $com_width = 900;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'News';
    }
public static function trueParam(){
    return \frontend\models\Comment($model->id_news);
}

/**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'descript', 'body', 'url', 'url_text', 'url_pict'], 'required'],
            [['name', 'descript', 'body', 'url', 'url_text', 'url_pict'], 'string'],
            [['crdate', 'putdate'], 'safe'],
            [['id_category', 'hide', 'top', 'blood', 'photo'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_news' => 'Id News',
            'name' => 'Name',
            'descript' => 'Descript',
            'body' => 'Body',
            'crdate' => 'Crdate',
            'putdate' => 'Putdate',
            'url' => 'Url',
            'url_text' => 'Url Text',
            'url_pict' => 'Url Pict',
            'id_category' => 'Category Idcategory',
            'hide' => 'Hide',
            'top' => 'Top',
            'blood' => 'Blood',
            'photo' => 'Photo',
        ];
    }
}

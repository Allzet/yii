<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "blog".
 *
 * @property integer $id
 * @property integer $id_user
 * @property string $name
 * @property string $text
 * @property integer $status
 * @property string $datecreate
 */
class Blog extends \yii\db\ActiveRecord {

    const IS_ACTIVE = 1;
    const IS_DEACTIVE = 0;
    public $params_user;
    

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'blog';
    }
    

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_user', 'name', 'text'], 'required'],
            [['status','id_user'], 'integer'],
            [['name', 'text',], 'string'],
            ['datecreate', 'default', 'value' => date('Y-m-d H:i:s')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'Ідентифікатор Запису',
            'id_user' => 'Юзер',
            'name' => 'Назва',
            'text' => 'Текст',
            'status' => 'Статус',
            'datecreate' => 'Дата та час створення',
        ];
    }

}

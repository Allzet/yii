<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\News;

/**
 * NewsSearch represents the model behind the search form about `frontend\models\News`.
 */
class NewsSearch extends News
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_news', 'id_category', 'hide', 'top', 'blood', 'photo'], 'integer'],
            [['name', 'descript', 'body', 'crdate', 'putdate', 'url', 'url_text', 'url_pict'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = (new \yii\db\Query)->select([
            'id_news',
            'name',
            'descript',
            'body',
            'crdate',
            'putdate' ,
            'url' ,
            'url_text' ,
            'url_pict',
            'id_category' ,
            'hide' ,
            'top' ,
            'blood' ,
            'photo' ,
        ])->from('News')
                //->where(['top'=>  self::IS_TOP,'hide'=>  self::IS_SHOW])
                ;
$query ->createCommand()-> rawSql;
//exit($query ->createCommand()-> rawSql);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_news' => $this->id_news,
            'crdate' => $this->crdate,
            'putdate' => $this->putdate,
            'id_category' => $this->id_category,
            'hide' => $this->hide,
            'top' => $this->top,
            'blood' => $this->blood,
            'photo' => $this->photo,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'descript', $this->descript])
            ->andFilterWhere(['like', 'body', $this->body])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'url_text', $this->url_text])
            ->andFilterWhere(['like', 'url_pict', $this->url_pict]);
//exit($query ->createCommand()-> rawSql);
        return $dataProvider;
    }
}

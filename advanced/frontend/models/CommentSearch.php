<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Comment;

/**
 * CommentSearch represents the model behind the search form about `frontend\models\Comment`.
 */
class CommentSearch extends Comment {
    public static $childComments;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_com', 'id_news', 'id_last'], 'integer'],
            [['date_cr',  'email', 'text','username'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Comment::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_com' => $this->id_com,
            'id_news' => $this->id_news,
            'id_last' => $this->id_last,
            'date_cr' => $this->date_cr,
           
            'username' => $this->username,
            
           
        ]);

        $query->andFilterWhere(['like', 'email', $this->email])
                ->andFilterWhere(['like', 'text', $this->text]);

        return $dataProvider;
    }

    public static function commentFromNews($id_news) {

        $query = (new \yii\db\Query())
                ->select('c.*,u.username')
                ->from('comment c')
                ->where([
                    'c.id_news' => $id_news,
                    'id_last' => 0,
                    ])
                ->leftJoin('user u','c.username=u.username' );
//echo $query->createCommand()->rawSql;
//exit;
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => array('pageSize' => 2),
        ]);


        return $dataProvider;
    }
    public static function commentNumberToNews($id_news) {

        $query = (new \yii\db\Query())
                ->select('text')
                ->from('comment')
                ->where([
                    'id_news' => $id_news,
                    
                    ])
                ->andWhere('id_last = 0')
                ;
//echo $query->createCommand()->rawSql;
//exit;
        $arr=$query->all();
      /// print_r($arr);

        return count($arr);
    }
    public static function commentParam($param) {

        $query = (new \yii\db\Query())
                ->select('nas_znach')
                ->from('skynas')
                ->where([
                    'nas_par' => $param,
                    
                    ])
                ;
//echo $query->createCommand()->rawSql;
//exit;
        
        $arr=  json_encode($query->all());
        $arr= explode('"', $arr);
        $zn=$arr[3];

        return $zn;
    }
    public static function commentChild($id_news) {
if (!empty(self::$childComments)){
return self::$childComments;
}
        $query = (new \yii\db\Query())
               ->select('c.*,u.username')
                ->from('comment c')
                ->where([
                    'c.id_news' => $id_news,
                
                    ])
               // ->andWhere('id_last > 0')
                ->leftJoin('user u','c.username=u.username' );
        
//echo $query->createCommand()->rawSql;
//exit;
        $arr=$query->all();
        self::$childComments=  self::transform2forest($arr, 'id_com', 'id_last');
        return self::$childComments;
     //     echo '<pre>';
     //   print_r($res);
      //  echo '</pre>';
      //  exit;
//echo $query->createCommand()->rawSql;
//exit;
     
    }
        public static function transform2forest($rows, $idName, $pidName, $childrenName = 'children')
    {

        $children = array(); // children of each ID
        $ids = array();
        foreach ($rows as $i => $r) {
            $row = &$rows[$i];
            $id = $row[$idName];
            $pid = $row[$pidName];
            $children[$pid][$id] = &$row;
            if (!isset($children[$id]))
                $children[$id] = array();
            $row[$childrenName] = &$children[$id];
            $ids[$row[$idName]] = true;
        }
        // Root elements are elements with non-found PIDs.
        $forest = array();
        foreach ($rows as $i => $r) {
            $row = &$rows[$i];
            if (!isset($ids[$row[$pidName]])) {
                $forest[$row[$idName]] = &$row;
            }
            #unset($row[$idName]); unset($row[$pidName]);
        }
        return $forest;
    }
    

}

<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'main.css',
        'croppic.css',
//'bootstrap.css',
            //	'css/st.css'
    ];
    public $js = [

//'js/jquery.js',
//'js/jquery.validationengine.js',
        'js/skycom.js',
//'jquery.pjax.js',
        'main.js',
        'jquery.mousewheel.min.js',
        'bootstrap.min.js',
        'croppic.min.js',
        'croppic.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}

<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\News;

class Newssearch extends News {

    public function rules() {
        return [
            [['id_news', 'id_category'], 'integer'],
            [['name', 'descript', 'body', 'putdate', 'url', 'url_text', 'url_pict', 'hide', 'top', 'blood', 'photo'], 'safe'],
        ];
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function isNewsByName($name) {

        $query = (new \yii\db\Query())
                ->select('id_news')
                ->from('news')
                ->where([
            'name' => $name,
        ]);
        $count = $query->count();

        return $count > 0 ? true : false;
    }

    public function search($params) {
        $query = News::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_news' => $this->id_news,
            //  'putdate' => $this->putdate,
            'id_category' => $this->id_category,
        ]);
        if ($this->putdate) {

            list($dateFrom, $dateTo) = explode(' - ', $this->putdate);

            if (!empty($dateFrom) && !empty($dateTo)) {

                $query->andFilterWhere(['between', self::tableName() . '.putdate', date('Y-m-d', strtotime($dateFrom)), date('Y-m-d 23:59:59', strtotime($dateTo))]);
            }

            unset($dateFrom, $dateTo);
        }

        $query->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'descript', $this->descript])
                ->andFilterWhere(['like', 'body', $this->body])
                ->andFilterWhere(['like', 'url', $this->url])
                ->andFilterWhere(['like', 'url_text', $this->url_text])
                ->andFilterWhere(['like', 'url_pict', $this->url_pict])
                ->andFilterWhere(['like', 'hide', $this->hide])
                ->andFilterWhere(['like', 'top', $this->top])
                ->andFilterWhere(['like', 'blood', $this->blood])
                ->andFilterWhere(['like', 'photo', $this->photo]);

        return $dataProvider;
    }

}

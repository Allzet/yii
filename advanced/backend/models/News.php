<?php

namespace backend\models;
use file\Manager\FileFactory;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property integer $id_news
 * @property string $name
 * @property string $descript
 * @property string $body
 * @property string $putdate
 * @property string $url
 * @property string $url_text
 * @property string $url_pict
 * @property integer $id_category
 * @property string $hide
 * @property string $top
 * @property string $blood
 * @property string $photo
 */
class News extends \yii\db\ActiveRecord {

    const IS_SHOW = 1;
    const IS_HIDE = 0;
    const IS_TOP = 1;
    const IS_NOTOP = 0;
    const IS_BLOOD = 1;
    const IS_NOBLOOD = 0;

    public $pict;
    public $url_pics;
    public $data;
    public static $basepathloc;
    public $atributeXMLReNane = [
        'title' => 'name',
        'description' => 'descript',
        'link' => 'url',
      
    ];

    public function getData() {
        return $this->data;
    }

    public function setData($data) {
        $this->data = $data;
        return $this;
    }

        public static function tableName() {
        return 'news';
    }

    public function rules() {
        return [
            [['name', 'descript', 'body', 'url', 'url_text', 'id_category'], 'required'],
            [['name', 'descript', 'body', 'url', 'url_text', 'hide', 'top', 'blood'], 'string'],
            [['url_pict'], 'safe'],
            ['putdate', 'default', 'value' => new \yii\db\Expression('now()')],
            ['crdate', 'default', 'value' => date('Y-m-d H:i:s')],
            [['id_category', 'photo'], 'integer']
        ];
    }

    public function attributeLabels() {
        return [
            'id_news' => 'Id Новини',
            'name' => 'Назва',
            'descript' => 'Опис',
            'body' => 'Основний текст',
            'putdate' => 'Дата та час випуску',
            'url' => 'Посилання',
            'url_text' => 'Текст посилання',
            'url_pict' => 'Зображення',
            'id_category' => 'Рубрика',
            'hide' => 'Статус',
            'top' => 'Головна',
            'blood' => 'Виділення',
            'photo' => 'Фоторепортаж',
            'pict' => 'Зображення',
            'home' => 'Головна',
        ];
    }

    public function behaviors() {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }

    public function createNews() {
        if (empty($this->data)) {
            return false;
        }
        $this->setAttributes($this->data);
        $this->save();
        $options = Yii::$app->params;
        if (!empty($this->data['image'])) {

            Yii::setAlias('@file', dirname(dirname(__DIR__)) . '/backend/modules/file');

            $fileManager = FileFactory::factory((object) $options['fileManager']);

            $fileManager->setAsset('newspars', ['created' => $this->crdate]);

            $fileManager->prepareStorage();

            $fileManager->prepareUploader(['download'=>true, 'url'=>$this->data['image']]);

            $fileManager->download($this->data['image']);

            $this->url_pict = serialize((array)$fileManager->getUploaded());

            $this->save();
        }
        $ifcli = php_sapi_name();
        if ($ifcli == 'cli') {
            News::$basepathloc="php/os/OpenServer/domains/yii-ad.local/advanced/backend/web/resources/images/news";
}else{
    News::$basepathloc="/resources/images/news";
}
        print_r($this->getErrors());
    }

    public function prepareDataFromXML() {
        if (empty($this->data)) {
            return $this;
        }
        $data = [];
        foreach ($this->data as $k => $v) {
            if (isset($this->atributeXMLReNane[(string) $k])) {
                $data[$this->atributeXMLReNane[(string) $k]] = (string) $v;
            }
            if ((string) $k == 'category') {
                $data['id_category'] = Category::getCategoryIdByName((string) $v);
            }
            if ((string) $k == 'link') {
                $data['body'] = $this->getBodyByLink((string) $v);
            }
            if ((string) $k == 'enclosure') {
                $data['image'] = (string) $v->attributes()->url;
            }
        }
        $data['url_text'] = $data['name'];
        print_r($this->data);
        $this->data = $data;
        print_r($this->data);
        return $this;
    }

    private function getBodyByLink($link) {
        $content = file_get_contents($link);
        preg_match('/<p>(.+)<\/p><\/div><div class="details">/', $content, $matches);
        return $matches[1];
    }

}

<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property integer $idcategory
 * @property string $categorycol
 */
class Category extends \yii\db\ActiveRecord {

    const DEFAULT_CATEGORY_ID = 3;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['categorycol'], 'required'],
            [['categorycol'], 'string', 'max' => 40]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'idcategory' => 'ID категорії',
            'categorycol' => 'Назва категорії',
            
        ];
    }

    public static function getCategoryIdByName($name) {
        if (empty($name)) {
            return self::DEFAULT_CATEGORY_ID;
        }
        $query = (new \yii\db\Query())
                ->select('idcategory')
                ->from('category')
                ->where([
            'categorycol' => $name,
        ]);
        $category = $query->one();

        if (!empty($category)) {
            return $category['idcategory'];
        } else {
            return self::DEFAULT_CATEGORY_ID;
        }
    }

}

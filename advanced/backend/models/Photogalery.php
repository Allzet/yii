<?php

namespace backend\models;

class Photogalery extends \yii\db\ActiveRecord {

    public $picts;
    public $desc;

    public static function tableName() {
        return 'Photogalery';
    }

    public function rules() {
        return [
            ['date_create', 'default', 'value' => date('Y-m-d H:i:s')],
            [['name_album'], 'required'],
            [['name_album'], 'string'],
            [['arr_pictures'], 'string'],
            [['desc_pictures'], 'string'],
        ];
    }

    public function checkIsArray() {
        if (!is_array($this->desc)) {
            $this->addError('desc', 'desc is not array!');
        }
    }

    public function attributeLabels() {
        return [
            'id_album' => 'ID фоторепортажу',
            'date_create' => 'Дата створення',
            'name_album' => 'Назва фоторепортажу',
        ];
    }

}

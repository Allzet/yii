<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Photogalery;

class PhotogalerySearch extends Photogalery
{
    public function rules()
    {
        return [
            [['id_album'], 'integer'],
            [['date_create', 'name_album'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Photogalery::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            'id_album' => $this->id_album,
            'date_create' => $this->date_create,
        ]);
        $query->andFilterWhere(['like', 'name_album', $this->name_album]);
        return $dataProvider;
    }
}

<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id_com
 * @property integer $id_news
 * @property integer $id_last
 * @property string $date_cr
 * @property string $date_ed
 * @property integer $user
 * @property integer $ident
 * @property string $email
 * @property string $text
 * @property integer $mod
 *
 * @property News $idNews
 * @property User $user0
 */
class Comment extends \yii\db\ActiveRecord
{
    public static $id_new;
    public static $id_coment;
    public static $com_width = 900;
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;
    

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * @inheritdoc
     */
     public function rules()
    {
        return [
            [[
                'id_news',
                'id_last', 'username', 'email', 'text',], 'required'],
            [['id_news', 'id_last', 'status', ], 'integer'],
            [['date_cr', ], 'safe'],
            [['email', 'text', 'username', 'ip'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
     public function attributeLabels()
    {
        return [
            'id_com' => 'ID Коментара',
            'id_news' => 'ID Новини',
            'id_last' => 'ID Батьківський',
            'username' => 'Логін',
            'email' => 'Email',
            'text' => 'Текст коментара',
            'status' => 'Статус',
            'ip' => 'IP',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdNews()
    {
        return $this->hasOne(News::className(), ['id_news' => 'id_news']);
    }
    public function getIdCrdate()
    {
        return $this->hasOne(News::className(), ['crdate' => 'crdate']);
    }
   // public $getForm = render('_form'); 
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser0()
    {
        return $this->hasOne(User::className(), ['username' => 'username']);
    }
}

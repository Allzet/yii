<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $role
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class User extends \yii\db\ActiveRecord
{
    const IS_ACTIVE = 1;
    const IS_DEACTIVE = 0;
    const IS_USER = 4;
    const IS_BLOG = 6;
    const IS_EDIT = 8;
    const IS_ADMIN = 10;
    public $picture;





    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'User';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['datecreate', 'default', 'value' => date('Y-m-d H:i:s')],
            [['username', ], 'required'],
            [['role', 'status', 'created_at', 'updated_at'], 'integer'],
            [['username', 'password_hash', 'password_reset_token', 'email', 'name', 'surname'
                , 'photo'
                ], 'string'],
            [['auth_key',], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Логін',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'role' => 'Роль',
            'status' => 'Статус',
            'created_at' => 'Дата реєстрації',
            'updated_at' => 'Updated At',
            'name' => "Ім'я",
            'surname' => 'Прізвище',
            'photo' => 'Аватар',
            'datecreate'=>'Дата створення',
            'picture' => 'Аватарка',

            
        ];
    }
}

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\CommentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comment-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_com') ?>

    <?= $form->field($model, 'id_news') ?>

    <?= $form->field($model, 'id_last') ?>

    <?= $form->field($model, 'date_cr') ?>

    <?= $form->field($model, 'date_ed') ?>

    <?php // echo $form->field($model, 'user') ?>

    <?php // echo $form->field($model, 'ident') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'text') ?>

    <?php // echo $form->field($model, 'mod') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

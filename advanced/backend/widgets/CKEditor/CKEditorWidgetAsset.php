<?php
/**
 * @copyright Copyright (c) 2014 2amigOS! Consulting Group LLC
 * @link http://2amigos.us
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */
namespace backend\widgets\CKEditor;

use yii\web\AssetBundle;

/**
 * CKEditorWidgetAsset
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @link http://www.ramirezcobos.com/
 * @link http://www.2amigos.us/
 * @package dosamigos\ckeditor
 */
class CKEditorWidgetAsset extends AssetBundle
{
	public $sourcePath = '@backend/widgets/CKEditor/assets';

	public $depends = [
		'backend\widgets\CKEditor\CKEditorAsset',
	];

	public $js = [
		'js/dosamigos-ckeditor.widget.js'
	];
} 
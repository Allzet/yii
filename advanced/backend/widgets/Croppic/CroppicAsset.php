<?php

namespace backend\widgets\Croppic;

use yii\web\AssetBundle;

class CroppicAsset extends AssetBundle {

    public $sourcePath = '@backend/widgets/Croppic/assets';
    public $js = [
        'js/bootstrap.min.js',
        'js/main.js',
        'js/jquery.mousewheel.min.js',
        'js/croppic.js',
        'js/croppic.min.js',
    ];
    public $css = [
        'css/croppic.css',
        'css/bootstrap.css',
        'css/main.css',
    ];
    public $depends = [
     //   'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

}

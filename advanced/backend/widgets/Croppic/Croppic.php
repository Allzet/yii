<?php

namespace backend\widgets\Croppic;

use yii\helpers\Html;
use yii\widgets\InputWidget;

class Croppic extends InputWidget {

    public function init() {
       // parent::init();
      //  $this->initOptions();
    }

    public function run() {
//        if ($this->hasModel()) {
//            echo Html::activeTextarea($this->model, $this->attribute, $this->options);
//        } else {
//            echo Html::textarea($this->name, $this->value, $this->options);
//        }
//        $this->registerPlugin();
        $element = Html::tag('div', '', [
                    'id' => $this->id,
        ]);
        $this->jsRegister();
        echo $element;
    }

    protected function registerPlugin() {
//        $view = $this->getView();
//
//        CKEditorWidgetAsset::register($view);
//
//        $id = $this->options['id'];
//
//        $options = $this->clientOptions !== false && !empty($this->clientOptions) ? Json::encode($this->clientOptions) : '{}';
//
//        $js[] = "CKEDITOR.replace('$id', $options);";
//        $js[] = "dosamigos.ckEditorWidget.registerOnChangeHandler('$id');";
//
//        if (isset($this->clientOptions['filebrowserUploadUrl'])) {
//            $js[] = "dosamigos.ckEditorWidget.registerCsrfImageUploadHandler();";
//        }
//
//        $view->registerJs(implode("\n", $js));
    }

    public function jsRegister() {

        $this->getView()->registerJs("
                $(function(){
                    var ". $this->id ."Options = {
                        uploadUrl:'img_save_to_file.php',
                        cropUrl:'img_crop_to_file.php',
                        modal:true,
                        imgEyecandyOpacity:0.4,
                        loaderHtml:'<div class=\"loader bubblingG\"><span id=\"bubblingG_1\"></span><span id=\"bubblingG_2\"></span><span id=\"bubblingG_3\"></span></div> '
                    }
                    var ". $this->id ." = new Croppic('". $this->id ."', ". $this->id ."Options);
                    
                });", \yii\web\View::POS_READY);

        return $this;
    }

}

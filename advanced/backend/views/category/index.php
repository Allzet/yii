<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\ActiveForm;

$this->title = 'Категорії';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <table style="text-align: left; width: 100%;" border="0"
           cellpadding="10" cellspacing="2">
        <tbody>
            <tr>
                <td
                    style="height: 40px; text-align: left; vertical-align: bottom; width: 150px;">
                        <?php
                        $form = ActiveForm::begin([
                                    'action' => ['index'],
                                    'method' => 'post',
                        ]);
                        echo $form->field($model, 'categorycol')->textInput();
                        ?>      
                </td>
                <td
                    style="width: 10px; text-align: left; vertical-align: bottom;">
                </td>
                <td
                    style="width: 200px; text-align: left; vertical-align: bottom;">
                    <div class="form-group">
                        <?php echo Html::submitButton('Додати категорію', ['class' => 'btn btn-success']) ?>
                    </div>                   
                    <?php ActiveForm::end(); ?>      
                </td>

            </tr>
        </tbody>
    </table>

    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'idcategory' => [
                'attribute' => 'idcategory',
                'value' => function ($model) {

                    return $model['idcategory'];
                },
                'options' => [
                    'style' => 'width : 40px;',
                ],
            ],
            ['class' => 'kartik\grid\EditableColumn',
                'header' => 'Категорія',
                'vAlign' => 'middle',
                'width' => '80%',
                'attribute' => 'categorycol',
                'editableOptions' => [
                    'header' => 'Категорія',
                    'size' => 'md',
                ]
            ],
            ['class' => 'backend\widgets\Knopki\Knopki',
                'options' => [
                    'style' => 'width : 5px;',
                ],
            ],
        ],
    ]);
    ?>   


</div>

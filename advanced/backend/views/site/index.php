<?php

use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Панель керування';
?>
<div class="site-index">
    <?php
    echo Html::a('Стрічка новин', ['/news'], ['class'=>'btn btn-primary']);
    echo '<br><br>';
    echo Html::a('Фотогалерея', ['/photogalery'], ['class'=>'btn btn-primary']);
    echo '<br><br>';
    echo Html::a('Коментарі', ['/comment'], ['class'=>'btn btn-primary']);
    echo '<br><br>';
    echo Html::a('Робота з користувачами', ['/user'], ['class'=>'btn btn-primary']);
    echo '<br><br>';
    echo Html::a('Категорії', ['/category'], ['class'=>'btn btn-primary']);
    //echo Html::a('Стрічка новин', ['/news'], ['class'=>'btn btn-primary']);
    
    ?>
</div>

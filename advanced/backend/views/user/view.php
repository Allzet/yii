<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\widgets\AllzetView\AllzetView;

/* @var $this yii\web\View */
/* @var $model backend\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php
    echo '<img src="' . \common\helpers\Image::image('user', unserialize($model->photo), 'preview', ['created' => $model->datecreate]) . '">';
    if (Yii::$app->user->identity->role == 10) {

    echo '<p>';
        echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
        echo '<br><br>';
        echo Html::a('Delete', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
        'confirm' => 'Are you sure you want to delete this item?',
        'method' => 'post',
        ],
        ]);

        echo '</p>';
    echo DetailView::widget([
    'model' => $model,
    'attributes' => [
    'id',
    'username',
    'name',
    'surname',
    // 'auth_key',
    // 'password_hash',
    // 'password_reset_token',
    'email:email',
    'role',
    'status',
    'created_at:date',
    // 'updated_at',
    ],
        
    ]);
    } else {

    if ($model->username == Yii::$app->user->identity->username) {
    echo '<p>';
        echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
        echo '</p>';
    echo DetailView::widget([
    'model' => $model,
    'attributes' => [
    'username',
    'name',
    'surname',
    'email:email',
    ],
    ]);
    } else {

    echo DetailView::widget([
    'model' => $model,
    'attributes' => [
    'username',
    'name',
    'surname',
    ],
    ]);
    }
    }
    ?>

</div>

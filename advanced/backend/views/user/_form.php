<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\slider\Slider;
use kartik\widgets\SwitchInput;
use kartik\widgets\FileInput;


/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>


    <?php
   // echo \backend\widgets\Croppic\Croppic::widget(['id'=> 123]);
    if (empty($model->username) or $model->username == Yii::$app->user->identity->username) {
        echo $form->field($model, 'username')->textInput();
        echo $form->field($model, 'name')->textInput();
        echo $form->field($model, 'surname')->textInput();
        echo $form->field($model, 'email')->textInput();
       
    } else {
        echo $form->field($model, 'username', ['inputOptions' => [ 'value' => $model->username]])->hiddenInput();
        echo $model->username . '<br><br>';
        echo $form->field($model, 'name', ['inputOptions' => [ 'value' => $model->name]])->hiddenInput();
        echo $model->name . '<br><br>';
        echo $form->field($model, 'surname', ['inputOptions' => [ 'value' => $model->surname]])->hiddenInput();
        echo $model->surname . '<br><br>';
        echo $form->field($model, 'email', ['inputOptions' => [ 'value' => $model->email]])->hiddenInput();
        echo $model->email . '<br><br><br>';
    }
    
    if (Yii::$app->user->identity->role ==10){
     // echo Yii::$app->user->identity->role;
    echo $form->field($model, 'role')->widget(Slider::classname(), [
        // 'name'=>'rating_5',
        // 'value'=>2,
        'pluginOptions' => [
            'min' => 4,
            'max' => 10,
            'step' => 2,
            'tooltip' => 'always',
            'formatter' => new yii\web\JsExpression("function(val) { 
            if (val == 4) {
                return 'Юзер';
            }
            if (val == 6) {
                return 'Блогер';
            }
            if (val == 8) {
                return 'Редактор';
            }
            if (val == 10) {
                return 'Адмін';
            }
            
        }")
        ]
    ]);

    echo $form->field($model, 'status')->widget(SwitchInput::classname(), [
        'pluginOptions' => [
            'handleWidth' => 80,
            'onText' => 'Активна',
            'offText' => 'Неактивна']
    ]); 
    }
    if ($model->username == Yii::$app->user->identity->username or Yii::$app->user->identity->role == 10) {
        echo $form->field($model, 'photo')->widget(FileInput::classname(), [
                        'options'       => [
                        'accept'      => 'image/*',
                        'multiple'    => TRUE,
                        'showRemove'  => FALSE,
                        'showUpload'  => FALSE,
                        'showCaption' => FALSE,
                    ],
                    'pluginOptions' => ['previewFileType' => 'any']
                ]);
    }
    
    //echo $form->field($model, 'role')->textInput() 
    //echo $form->field($model, 'auth_key')->textInput(['maxlength' => 32]) 
    //echo $form->field($model, 'password_hash')->textInput(['maxlength' => 255]) 
    //echo $form->field($model, 'password_reset_token')->textInput(['maxlength' => 255]) 
    //echo $form->field($model, 'email')->textInput(['maxlength' => 255]) 
    // $model->status = true;
    //echo $form->field($model, 'status')->textInput() 
    // echo $form->field($model, 'created_at')->textInput() 
    // echo $form->field($model, 'updated_at')->textInput() 
    ?>

    <div class="form-group">
<?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Зберегти', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>

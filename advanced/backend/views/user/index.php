<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id' => [
                'attribute' => 'id',
                'value' => function ($model) {

                    return $model['id'];
                },
                'options' => [
                    'style' => 'width : 40px;',
                ],
            ],
            'username',
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
            'email:email',
            'role' => [
                'attribute' => 'role',
                'filter' => [
                    \backend\models\User::IS_USER => 'юзер',
                    \backend\models\User::IS_BLOG => 'блогер',
                    \backend\models\User::IS_EDIT => 'редактор',
                    \backend\models\User::IS_ADMIN => 'адміністратор',
                ],
                'value' => function ($model) {

            $status = [
                \backend\models\User::IS_USER => 'юзер',
                \backend\models\User::IS_BLOG => 'блогер',
                \backend\models\User::IS_EDIT => 'редактор',
                \backend\models\User::IS_ADMIN => 'адміністратор',
            ];

            return isset($status[$model['role']]) ? $status[$model['role']] : '';
        },
                'options' => [
                    'style' => 'width : 140px; text-align: center;',
                ],
            ],
            'status' => [
                'attribute' => 'status',
                'filter' => [
                    \backend\models\User::IS_ACTIVE => 'активна',
                    \backend\models\User::IS_DEACTIVE => 'неактивна',
                ],
                'value' => function ($model) {

            $status = [
                \backend\models\User::IS_ACTIVE => 'активна',
                \backend\models\User::IS_DEACTIVE => 'неактивна',
            ];

            return isset($status[$model['status']]) ? $status[$model['status']] : '';
        },
                'options' => [
                    'style' => 'width : 140px; text-align: center;',
                ],
            ],
            'created_at:date',
            // 'updated_at',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>

</div>

<?php

use yii\helpers\Html;
use yii\helpers\Url;
use demogorgorn\ajax\AjaxSubmitButton;

\backend\models\Comment::$id_new = $model['id_news'];
\backend\models\Comment::$id_coment = $model['id_com'];
$countcom = \backend\models\CommentSearch::commentNumberToNews($model->id_news);
$com_width = $com_width - 15;
$ncom = $model ['id_com'];
$status = Yii::$app->getRequest()->getQueryParams();

?>

<strong><?php echo $model ['username']; ?></strong><br>

<div id="text_<?php echo $model['id_com']; ?>">
    <?php
    echo Yii::$app->controller->renderPartial('comment-text', ['model' => $model]);
    ?>
</div>
<?php
echo Html::beginForm('', 'post', ['class' => 'uk-width-medium-1-1 uk-form uk-form-horizontal']);

echo Html::input('hidden', 'comment_id', $model['id_com']);

AjaxSubmitButton::begin([
    'label' => 'Статус',
    'ajaxOptions' => [
        'type' => 'POST',
        'url' => Url::toRoute(['comment/getinfo']),
        /* 'cache' => false, */
        'success' => new \yii\web\JsExpression('function(html){
            if (html == 1){
                $("#text_' . $model['id_com'] . '").children("span").css("color", "#333");
                }else{
                $("#text_' . $model['id_com'] . '").children("span").css("color", "#D3D3D3");
                }
            }'),
    ],
    'options' => ['class' => 'customclass', 'type' => 'submit'],
]);
AjaxSubmitButton::end();

echo Html::endForm();
?>
<a name="nach" id="ncom"></a> <a style="cursor:pointer;" onClick="showlayer('komm<?php echo $ncom ?>')">Прокоментувати</a>

<div id="komm<?php echo $ncom; ?>" style="display:none; padding:10px;  width:<?php echo $com_width; ?>;" align="left">
    <?php
    echo $model['id_com'];
    echo $this->render('/comment/create', ['model' => new \backend\models\Comment(),]);
    ?>
</div>
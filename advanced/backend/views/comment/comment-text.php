<?php

use kartik\editable\Editable;


    ?>
    <span style="<?php if (empty($model['status'])):?> color:#D3D3D3 <?php endif;?>">
    <?php

echo 'IP: ' . $model['ip'] . '<br>';
    echo Editable::widget([
        'name' => "notes[{$model['id_com']}]",
        // 'displayValue' => '',
        'format' => Editable::FORMAT_BUTTON,
        'inputType' => Editable::INPUT_TEXTAREA,
        'value' => $model ['text'],
        'header' => 'Коментар',
        'submitOnEnter' => TRUE,
        'size' => 'lg',
        'id' => 'comment_text_' . $model['id_com'],
        'options' => [
            'class' => 'form-control', 'rows' => 5, 'placeholder' => 'Введіть текст...']
    ]);?>
</span>



 
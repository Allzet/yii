<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Comment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comment-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php // echo $form->field($model, 'id_last')->textInput() ?>
    <?php // echo $form->field($model, 'date_cr')->textInput() ?>
    <?php //echo $form->field($model, 'date_ed')->textInput() ?>    
    <?php //echo $form->field($model, 'mod')->textInput() ?>    

    <?php // echo $form->field($model, 'id_news')->textInput() ?>



    <?php // echo $form->field($model, 'ident')->textInput() ?>    

    <table style="text-align: left; width: 100%;" border="0"
           cellpadding="2" cellspacing="2">
        <tbody>
            <tr>
                <td style="width: 100px; text-align: left;"></td>
                <td style="text-align: left;">
                    <?php echo $form->field($model, 'username')->textInput() ?>
                </td>
                <td style="width: 100px; text-align: center;"></td>
            </tr>
            <tr>
                <td style="text-align: left;"></td>
                <td style="text-align: left;">
                    <?php echo $form->field($model, 'email')->textInput() ?>
                </td>
                <td style="text-align: center;"></td>
            </tr>
            <tr>
                <td style="text-align: left;" colspan="3"
                    rowspan="1">
                        <?php echo $form->field($model, 'text')->textarea(['rows' => 6]) ?> 
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td style="text-align: center;">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </td>
            </tr>
        </tbody>
    </table>




    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

?>

<div class="comment-search">

    <?php
    $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
    ]);
    echo
    $form->field($model, 'id_news')->widget(Select2::classname(), [
        'data' => yii\helpers\ArrayHelper::map(\backend\models\News::find()
                        ->orderBy(['crdate' => SORT_DESC])
                        ->asArray()
                        ->all()
                , 'id_news'
                , 'name'),
        'language' => 'ru',
        'options' => ['placeholder' => 'Оберіть Новину...'],
        'pluginOptions' => [
            'allowClear' => TRUE
        ],
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton('Пошук коментарів', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

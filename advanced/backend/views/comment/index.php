<?php

use yii\helpers\Html;
use yii\widgets\Pjax;

$this->title = 'Коментарі';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-index">

    <h1><?php echo Html::encode($this->title); ?></h1>

    <?php
    echo $this->render('_search', ['model' => $searchModel]);

    \backend\models\Comment::$id_coment = '0';
    $countcom = \backend\models\CommentSearch::commentNumberToNews(\backend\models\Comment::$id_new);
    $com_width = \backend\models\Comment::$com_width;

    if (!empty($countcom)) {
        echo 'Коментарів: ' . $countcom;
    } else {
        echo 'Коментарів нема';
    }
    ?>
    <br>
    <a name="nach" id="ncom"></a> <a style="cursor:pointer;" onClick="showlayer('komm')">Прокоментувати</a>
    <br><br>
    <?php
    echo '<div id="komm" style="display:none; padding:10px;  width:' . $com_width . '-15px ;" align="left">';
    echo $this->render('/comment/create', ['model' => new \backend\models\Comment()]);
    echo '</div>';

    Pjax::begin([
        'id' => 'comments'
    ]);
    echo $comments;
    Pjax::end();
    ?>   










</div>

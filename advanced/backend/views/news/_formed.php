<!-- NACHALO-->
<?php

use backend\widgets\CKEditor\CKEditor;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use kartik\widgets\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\sortinput\SortableInput;


/* @var $this yii\web\View */
/* @var $model backend\models\News */
/* @var $form yii\widgets\ActiveForm */
?>






<div class="news-form">
    <!--!!!!!!!!!!!!!!!!-->
    <div class="module-body">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <table cellspacing="1" cellpadding="1">


            <tr>
                <td style="width: 600px;">
                    <p>
                        <?= $form->field($model, 'name')->textarea(['rows' => 1]) ?>
                    </p>

                    <p>
                        <?= $form->field($model, 'descript')->textarea(['rows' => 4]) ?>
                    </p>

                    <p>
                        <?php //echo$form->field($model, 'body')->textarea(['rows' => 8]) ?>
                        <?php
                        echo $form->field($model, 'body')->widget(CKEditor::className(), [
                            'clientOptions' => [
                                'toolbarGroups'             => [
                                    ['name' => 'clipboard', 'groups' => ['mode', 'undo', 'selection', 'clipboard', 'doctools']],
                                    ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
//                '/',
                                    ['name' => 'paragraph', 'groups' => ['templates', 'list', 'indent', 'align']],
                                    ['name' => 'insert'],
//                '/',
                                    ['name' => 'editing', 'groups' => ['tools', 'about']],
                                    ['name' => 'links'],
                                    ['name' => 'others'],
                                    ['name' => 'colors'],
                                ],
                                'filebrowserBrowseUrl'      => '/ckeditor/kcfinder/browse.php?type=files',
                                'filebrowserImageBrowseUrl' => '/ckeditor/kcfinder/browse.php?type=images',
                                'filebrowserFlashBrowseUrl' => '/ckeditor/kcfinder/browse.php?type=flash',
                                'filebrowserUploadUrl'      => '/ckeditor/kcfinder/upload.php?type=files',
                                'filebrowserImageUploadUrl' => '/ckeditor/kcfinder/upload.php?type=images',
                                'filebrowserFlashUploadUrl' => '/ckeditor/kcfinder/upload.php?type=flash',
                            ],
                            'preset'        => 'custom',
                        ]);
                        ?>
                    </p>

                    <p>
                        <?= $form->field($model, 'url')->textInput() ?>
                    </p>

                    <p>
                        <?= $form->field($model, 'url_text')->textInput() ?>
                    </p>
                    <?php //echo $form->field($model, 'url_pict')->fileInput() ?>
                    <?php
                    echo $form->field($model, 'pict[]')->widget(FileInput::classname(), [
                        'options' => [
                            'accept'      => 'image/*',
                            'multiple'    => TRUE,
                            'showRemove'  => FALSE,
                            'showUpload'  => FALSE,
                            'showCaption' => FALSE,
                        ],
                    ]);
                    ?>


                </td>
                <td style="width: 20px; ">
                </td>
                <td style="width: 300px; vertical-align: top;">
                    <p>


                        <?php
                        echo $form->field($model, 'putdate', [
                            // 'value' => date('Y-m-d H:i:s'),
                        ])->widget(DateTimePicker::classname(), [
                            'options'       => ['placeholder' => date('Y-m-d H:i:s'), 'now'],
                            'value'         => date('Y-m-d H:i:s'),
                            'pluginOptions' => [
                                'autoclose'      => TRUE,
                                'startDate'      => date('Y-m-d H:i:s'),
                                'todayHighlight' => TRUE
                            ]]);
                        ?>
                        <?=
                        $form->field($model, 'id_category')->widget(Select2::classname(), [
                            'data'          => array_merge(["" => ""], yii\helpers\ArrayHelper::map(\backend\models\Category::find()
                                                                ->asArray()->all(), 'idcategory', 'categorycol')),
                            'language'      => 'ru',
                            'options'       => ['placeholder' => 'Оберіть рубрику...'],
                            'pluginOptions' => [
                                'allowClear' => TRUE
                            ],
                        ]);
                        ?>
                        <?= $form->field($model, 'hide')->checkbox() ?>

                        <?= $form->field($model, 'top')->checkbox() ?>

                        <?= $form->field($model, 'blood')->checkbox() ?>

                        <?php echo
                        $form->field($model, 'photo')->widget(Select2::classname(), [
                            'data'    => array_merge(["" => ""], yii\helpers\ArrayHelper::map(\backend\models\Photogalery::find()
                       //                         ->orderBy(['psefdomin' => SORT_ASC])
                                ->asArray()->all(), 'id_album', 'name_album')),
                            //'language' => 'ru',
                            'options' => ['placeholder' => 'Оберіть галерею...'],
                            //     'pluginOptions' => [
                            //     'allowClear' => TRUE
                            //      ],
                        ]);

                        //                        echo '<pre>';
                        //  print_r(yii\helpers\ArrayHelper::map(\backend\models\Photogalery::find()->orderBy(['id_album' => SORT_DESC])->asArray()->all(), 'arr_pictures', 'name_album'));
                        //   echo AllzetView::widget(['model' => $model, 'attributes' => ['photo'],]);
                        //  echo '</pre>';


                        //  echo '<pre>';
                        //  print_r(yii\helpers\ArrayHelper::map(\backend\models\Photogalery::find()
                        //      ->orderBy(['id_album' => SORT_DESC])->asArray()->all(), 'id_album', 'name_album'));
                        //  echo '</pre>';


                        ?>

                    <div class="news-form">
                        <div class="form-group">
                            <?= Html::submitButton($model->isNewRecord ? 'Створити' : 'Зберегти', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>


                    </div>
                    </p>

                </td>
            </tr>

            <tr>
                <td colspan="2">
                </td>
            </tr>
        </table>


        <?php
        // frame, rotate and save an image
        //Image::frame('path/to/image.jpg', 5, '666', 0)
        //   ->rotate(-8)
        //  ->save('path/to/destination/image.jpg', ['quality' => 50]);
        ?>


<?php ActiveForm::end(); ?>
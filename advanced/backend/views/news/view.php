<?php
use yii\helpers\Html;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Стрічка новин', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">
    <p>
        <?php
        echo Html::a('Редагувати', ['update', 'id' => $model->id_news], ['class' => 'btn btn-primary']);
        echo '  ';
        echo Html::a('Видалити', ['delete', 'id' => $model->id_news], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Новина буде повністю видалена. Продовжити?',
                'method' => 'post',
            ],
        ]);
        echo '  ';
        echo Html::a('До стрічки новин', ['index'], ['class'=>'btn btn-primary']);
                
        ?>
    </p>
    <table style="text-align: left; width: 100%;" border="0"
           cellpadding="2" cellspacing="2">
        <tbody>
            <tr>
                <td colspan="1" rowspan="4" style="text-align: left; vertical-align: top; width: 300px;">
                    <?php
                    echo 'Дата та час випуску: ' . $model->putdate;
                    echo '<br><br><img src="' . \common\helpers\Image::image('news', unserialize($model->url_pict)[0], 'thumb', ['created' => $model->crdate]) . '"><br>';
                    echo '<br>ID новини: ' . $model->id_news;
                    echo '<br><br>Рубрика: ' . backend\models\Category::findOne($model->id_category)->categorycol;
                    echo '<br><br>Посилання: <a href="' . $model->url . '">' . $model->url_text . '</a>';
                    echo '<br><br>Розташування: ';
                    echo $model->top == \backend\models\News::IS_TOP ? 'на головній' : 'у стрічці';
                    echo '<br><br>Вигляд у стрічці: ';
                    echo $model->blood == \backend\models\News::IS_BLOOD ? 'виділена' : 'звичайна';
                    echo '<br><br>Статус: ';
                    echo $model->hide == \backend\models\News::IS_SHOW ? 'активна' : 'не активна';
                    echo !empty($model->photo) ? '<br><br>Фоторепортаж' : '';
                    ?>
                </td>
                <th style=" width: 900px; text-align: center; vertical-align: top;">
        <h1>
            <?php echo $model->name . '<br><br>'; ?>
        </h1>
        </th>
        </tr>
        <tr>

            <th style="width: 900px; vertical-align: top;">
                <?php echo $model->descript . '<br><br>' ?>
            </th>
        </tr>
        <tr>
            <td style="width: 900px; vertical-align: top;">
                
                <?php
                echo $model->body . '<br><br>'; 
                ?>
            </td>
        </tr>
        <tr>
            <td style="width: 900px; text-align: center; vertical-align: top;">
                
                <?php 
                $gallery = \backend\models\Photogalery::findOne($model->photo);
                if (!empty($gallery['arr_pictures'])) {

                    $items = json_decode($gallery['arr_pictures']);
                    $itemdesc = json_decode($gallery['desc_pictures']);
                    $widget = backend\widgets\fotorama\Widget::begin([
                                'version' => '4.5.2',
                                'options' => [
                                    'nav' => 'thumbs',
                                ],
                                'htmlOptions' => [
                                    'class' => 'fotorama',
                                    'data-navposition' => 'top',
                                ],
                    ]);
                    foreach ($items as $k => &$v) {
                        $v = ['<img src="' . \common\helpers\Image::image('photogalery', $v, 'display', ['created' => $gallery['date_create']]) . '"' .
                            'data-caption="' . $itemdesc[$k] . '"' .
                            '>'];
                    }
                    print_r($items);
                    $widget->end();
                }
                ?>
            </td>
        </tr>
        </tbody>
    </table>
</div>

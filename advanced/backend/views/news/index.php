<?php

use kartik\daterange\DateRangePicker;
use kartik\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\Newssearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Стрічка новин';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Додати новину', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'putdate' => [
                'attribute' => 'putdate',
                'filterType' => DateRangePicker::className(),
                'filterWidgetOptions' => [
                    'useWithAddon' => TRUE,
                    'hideInput' => TRUE,
                    'presetDropdown' => TRUE,
                    'pluginOptions' => [
                        'format' => 'YYYY-MM-DD',
                        'separator' => ' - ',
                        'opens' => 'left',
                        'startDate' => date('Y-01-01'),
                        'endDate' => date('Y-m-d', strtotime('today')),
                    ],
                ],
                'options' => [
                    'style' => 'width : 120px; text-align: center;',
                ],
            ],
            // 'id_news',
            [
                'attribute' => 'name',
                'vAlign' => 'middle',
                'width' => '450px',
                'value' => function ($model, $key, $index, $widget) {
                    return Html::a($model->name, 'index.php?r=news%2Fview&id=' . $model->id_news, [
                                'title' => 'переглянути новину: ' . $model->name
                    ]);
                },
                        'format' => 'raw'
                    ],
                    //'descript:ntext',
                    //'body:ntext',
                    // 'url:ntext',
                    // 'url_text:ntext',
                    // 'url_pict:ntext',
                    // 'id_category',
                    'hide' => [
                        'attribute' => 'hide',
                        'filter' => [
                            \backend\models\News::IS_SHOW => 'активна',
                            \backend\models\News::IS_HIDE => 'не активна',
                        ],
                        'value' => function ($model) {

                    $status = [
                        \backend\models\News::IS_SHOW => 'активна',
                        \backend\models\News::IS_HIDE => 'не активна',
                    ];

                    return isset($status[$model['hide']]) ? $status[$model['hide']] : '';
                },
                        'options' => [
                            'style' => 'width : 140px; text-align: center;',
                        ],
                    ],
                    'top' => [
                        'attribute' => 'top',
                        'filter' => [
                            \backend\models\News::IS_TOP => 'на головній',
                            \backend\models\News::IS_NOTOP => 'у стрічці',
                        ],
                        'value' => function ($model) {

                    $status = [
                        \backend\models\News::IS_TOP => 'на головній',
                        \backend\models\News::IS_NOTOP => 'у стрічці',
                    ];

                    return isset($status[$model['top']]) ? $status[$model['top']] : '';
                },
                        'options' => [
                            'style' => 'width : 140px; text-align: center;',
                        ],
                    ],
                    'blood' => [
                        'attribute' => 'blood',
                        'filter' => [
                            \backend\models\News::IS_BLOOD => 'виділена',
                            \backend\models\News::IS_NOBLOOD => 'звичайна',
                        ],
                        'value' => function ($model) {

                    $status = [
                        \backend\models\News::IS_BLOOD => 'виділена',
                        \backend\models\News::IS_NOBLOOD => 'звичайна',
                    ];

                    return isset($status[$model['blood']]) ? $status[$model['blood']] : '';
                },
                        'options' => [
                            'style' => 'width : 140px; text-align: center;',
                        ],
                    ],
                    // 'photo',
                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]);
            ?>

</div>

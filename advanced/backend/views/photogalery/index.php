<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\daterange\DateRangePicker;

$this->title = 'Фотогалерея';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="photogalery-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Додати фоторепортаж', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id_album'=> [
                'attribute' => 'id_album',
                'value' => function ($model) {

                    return $model['id_album'];
                },
                'options' => [
                    'style' => 'width : 40px; text-align: center;',
                ],
            ],
            'date_create'=> [
                'attribute'           => 'date_create',
                'filterType'          => DateRangePicker::className(),
                'filterWidgetOptions' => [
                    'useWithAddon'   => TRUE,
                    'hideInput'      => TRUE,
                    'presetDropdown' => TRUE,
                    'pluginOptions'  => [
                        'format'    => 'YYYY-MM-DD',
                        'separator' => ' - ',
                        'opens'     => 'left',
                        'startDate' => date('Y-01-01'),
                        'endDate'   => date('Y-m-d', strtotime('today')),
                    ],
                ],
                'options'             => [
                    'style' => 'width : 170px; text-align: center;',
                ],
            ],
                        [
                'attribute' => 'name_album',
                'vAlign' => 'middle',
                'width' => '600px',
                'value' => function ($model, $key, $index, $widget) {
                    return Html::a($model->name_album, 'index.php?r=photogalery%2Fupdate&id=' . $model->id_album, [
                        'title'=>'переглянути/редагувати фоторепортаж ' . $model->name_album
                    ]);
                },
                        'format' => 'raw'
                    ],
           // 'name_album:ntext',
            ['class' => 'backend\widgets\Knopki\Knopki',
                'options' => [
                    'style' => 'width : 5px;',
                ],
            ],
        ],
    ]); ?>

</div>

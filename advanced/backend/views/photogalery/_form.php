<?php

use kartik\sortinput\SortableInput;
use kartik\widgets\FileInput;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
?>
<div class="photogalery-form">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
    <table style="text-align: left; width: 100%;" border="0"
           cellpadding="2" cellspacing="2">
        <tbody>
            <tr>
                <td
                    style="width: 600px; text-align: left; vertical-align: top;"
                    colspan="3" rowspan="1">
                        <?php echo $form->field($model, 'name_album')->textarea(['rows' => 1]) ?>
                </td>
                <td style="width: 15px;"></td>
                <td colspan="1" rowspan="3"
                    style="width: 600px; vertical-align: top;">
                        <?php
                        
                        if (!$model->isNewRecord) {
                            echo '| ID фоторепортажу: ' . $model->id_album . '  | <br>|  Дата та час створення: ' . $model->date_create . ' |';
                            if (!empty($model->arr_pictures)) {
                                $items = json_decode($model->arr_pictures);
                                $desc_arr = json_decode($model->desc_pictures);
                                foreach ($items as $k => &$v) {
                                    $v = ['content' => '<div>
            <img src="' . \common\helpers\Image::image('photogalery', $v, 'preview', ['created' => $model->date_create]) . '">
            <a href="' . Url::toRoute(['delete-img', 'id' => $model->id_album, 'imgid' => $k]) . '">'
                                        . '<span class = "glyphicon glyphicon-trash">&nbsp;</span></a>' .
                                        Html::input('text', 'desc[]', $desc_arr[$k]) .
                                        '</div>'];
                                }
                                echo SortableInput::widget([
                                    'name' => 'sort_list_2',
                                    'items' => $items,
                                    'hideInput' => true,
                                    'options' => ['class' => 'form-control', 'readonly' => TRUE],
                                ]);
                            }
                        }
                        ?>

                </td>
            </tr>
            <tr>
                <td
                    style="width: 200px; text-align: left; vertical-align: top;">
                        <?php
                        Modal::begin([
                            'header' => 'File Input inside Modal',
                            'toggleButton' => [
                                'label' => 'Завантажити фото', 'class' => 'btn btn-success'
                            ],
                        ]);
                        $form1 = ActiveForm::begin([
                                    'options' => ['enctype' => 'multipart/form-data'] // important
                        ]);
                        echo $form->field($model, 'picts[]')->widget(FileInput::classname(), [
                            'options' => [
                                'accept' => 'image/*',
                                'multiple' => TRUE,
                                'showRemove' => TRUE,
                                'showUpload' => FALSE,
                                'showCaption' => TRUE,
                            ],
                            'pluginOptions' => ['previewFileType' => 'any']
                        ]);
                        ActiveForm::end();
                        Modal::end();
                        ?> 
                </td>
                <td
                    style="width: 200px; text-align: center; vertical-align: top;">
                        <?php
                        echo Html::submitButton($model->isNewRecord ? 'Створити' : 'Зберегти', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
                        ?>      
                </td>
                <td
                    style="width: 200px; text-align: right; vertical-align: top;">
                        <?php
                        echo Html::a('Перейти до фотогалереї', ['index'], ['class'=>'btn btn-primary']);
                        ?>      
                </td>
                <td></td>
            </tr>
            <tr>
                <td style="text-align: center;" colspan="4"
                    rowspan="1"><br>
                        <?php
                        if (!$model->isNewRecord) {
                            if (!empty($model->arr_pictures)) {
                                $items = json_decode($model->arr_pictures);
                                $itemdesc = json_decode($model->desc_pictures);
                                $widget = backend\widgets\fotorama\Widget::begin([
                                            'version' => '4.5.2',
                                            'options' => [
                                                'nav' => 'thumbs',
                                            ],
                                            'htmlOptions' => [
                                                'class' => 'fotorama',
                                                'data-navposition' => 'top',
                                                'data-allowfullscreen' => 'native',
                                            ],
                                ]);
                                foreach ($items as $k => &$v) {
                                    $v = ['<img src="' . \common\helpers\Image::image('photogalery', $v, 'display', ['created' => $model->date_create]) . '"' .
                                        'data-caption="' . $itemdesc[$k] . '"' .
                                        '>'];
                                }
                                print_r($items);
                                $widget->end();
                            }
                        }
                        ActiveForm::end();
                        ?>
                </td>
            </tr>
        </tbody>
    </table>


</div>

<?php

namespace file\Image;

class Generator {

    protected $_destination;
    protected $_fontPath;
    protected $_fontSize = 12;
    protected $_paddingWidth = 1;
    protected $_paddingHeigth = 2;
    protected $_angle = 0;

    public function setDestination($destination) {

        $this->_destination = $destination;

        return $this;
    }

    public function setFontPath($fontPath) {

        $this->_fontPath = $fontPath;

        return $this;
    }

    public function setFontSize($fontSize) {

        if(preg_match('/^(\d){1}/', GD_VERSION, $matches)) {
            if($matches[1] == 2) {
                $fontSize = ($fontSize * 3) / 4;
            }
        }

        $this->_fontSize = $fontSize;

        return $this;
    }

    public function setPaddingWidth($paddingWidth) {

        $this->_paddingWidth = $paddingWidth;

        return $this;
    }

    public function setPaddingHeigth($paddingHeigth) {

        $this->_paddingHeigth = $paddingHeigth;

        return $this;
    }

    public function setAngle($angle) {

        $this->_angle = $angle;

        return $this;
    }

    public function textToImage($text) {

        // IMAGE WIDTH/HEIGHT BY YOUR TEXT
        $box = imagettfbbox($this->_fontSize, $this->_angle, $this->_fontPath, $text);

        // верхний правый - верхний левый. х-координата
        $imageWidth = abs($box[4] - $box[6]) + ($this->_paddingWidth * 2);
        // нижний левый - верхний левый. у-координата
        $imageHeight = abs($box[1] - $box[7]) + ($this->_paddingHeigth * 2);

        $image = imagecreatetruecolor($imageWidth, $imageHeight);
        imagecolortransparent($image, imagecolorallocate($image, 0, 0, 0));
        imagealphablending($image, false);
        imagesavealpha($image, true);

        // COLORS: WHITE WITH ALPHA FOR BACKGROUND, BLACK FOR TEXT
        $backgroundColor = imagecolorallocatealpha($image, 255, 255, 255, 127);
        $textColor = imagecolorallocate($image, 0x46, 0x46, 0x46);

        // FILL IMAGE BACKGROUND WITH WHITE COLOR
        imagefill($image, 0, 0, $backgroundColor);

        // PUT TEXT ON IMAGE
        $x = $this->_paddingWidth;                       // с левой стороны
        $y = $imageHeight - ($this->_paddingHeigth + 3);  // с нижней стороны
        imagettftext($image, $this->_fontSize, $this->_angle, $x, $y, $textColor, $this->_fontPath, $text);

        // Степень сжатия. 0 - нет сжатия
        $quality = 0;
        
        imagepng($image, $this->_destination, $quality);
        imagedestroy($image);
    }

}


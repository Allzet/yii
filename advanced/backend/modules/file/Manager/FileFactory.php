<?php

namespace file\Manager;

use file\Manager\Adapter;

class FileFactory {

    private function __construct() {

    }

    public static function factory($options = null) {

        $adapterClass = 'file\\Manager\\Adapter\\' . ucfirst(strtolower($options->adapter));

        if (class_exists($adapterClass)) {
            if (!empty($options)) {
                $class = new $adapterClass($options);
            } else {
                $class = new $adapterClass();
            }
            return $class;
        } else {
            throw new Exception(
                    sprintf('Specified adapter \'%s\' is not available', $adapterClass));
        }

        return null;
    }

}
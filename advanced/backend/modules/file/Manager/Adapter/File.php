<?php

namespace file\Manager\Adapter;

use file\Image;
use file\Manager\Adapter;
use file\Manager\FileInterface;
use file\Uploader\Post;
use file\Uploader\Download;
use file\Uploader\Xhr;

class File implements FileInterface {

    protected $_options;
    protected $_theme;
    protected $_asset;
    protected $_source;
    protected $_destination;
    protected $_path;
    protected $_file;
    protected $_uploaded;
    protected $_original;
    protected $_variables;
    protected $_name;
    protected $_ext;

    /**
     * @return mixed
     */
    public function getAsset() {
        return $this->_asset;
    }

    public function __construct($options) {

        $options = (array) $options;
        $this->_options = $options;
    }

    public function getDocumentRoot() {

        return $this->_options['documentroot'];
    }

    public function prepareUploader($options = NULL) {
        if (!empty($options['download'])) {
            $this->_file = new Download($options);
        } elseif (strpos(strtolower($_SERVER['CONTENT_TYPE']), 'multipart/') === 0) {
            $this->_file = new Post($options);
        } else {
            $this->_file = new Xhr($options);
        }
    }

    public function setAsset($asset, $variables = array()) {

//        if (Zend_Registry::isRegistered("theme")) {
//            $this->_theme = Zend_Registry::get("theme");
//        }

        if (!array_key_exists($asset, $this->_options)) {
            throw new \Exception("Для $asset нет описания");
        }

        $this->_asset = $this->_options[$asset];
        $this->_variables = (array) $variables;

        if (array_key_exists('extends', $this->_asset)) {
            $extends = $this->_asset['extends'];
            $this->_asset = array_merge($this->_options[$extends], $this->_asset);
        }

        if (!array_key_exists('basepath', $this->_asset)) {
            throw new \Exception("Для $asset не указан параметр basepath");
        }

        if (!array_key_exists('rename', $this->_asset)) {
            $this->_asset['rename'] = TRUE;
        }

        $this->_prepareDestination();
    }

    public function getValidate() {

        $validate = array();

        if (array_key_exists('validate', $this->_asset)) {
            $validate = $this->_asset['validate'];
        }

        return $validate;
    }

    protected function _prepareDestination() {
        try {
            // BASEPATH
            $basepath = $this->_asset['basepath'];
            $basepath = preg_replace('/\/$/', '', $basepath);
            // PATH
            $path = $basepath;

            // DESTINATION
            $destination = $this->_options['documentroot'] . "/" . $basepath;

            // DESTINATION
            $pattern = NULL;
            if (array_key_exists('pattern', $this->_asset)) {
                $pattern = $this->_asset['pattern'];
                $pattern = preg_replace('/\/$/', '', $pattern);
            }
            if (!empty($pattern)) {
                $pattern = $this->_assemblePattern($pattern);
                $destination = $destination . "/" . $pattern;
                $path = $path . "/" . $pattern;
            }
            $this->_destination = $destination;
            $this->_path = $path;
        } catch (\Exception $e) {

            exit($e->getMessage());
        }
    }

    private function _getPathInfo($filename) {

        $pathinfo = pathinfo($filename);
        $this->_name = @$pathinfo['filename'];
        $ext = @$pathinfo['extension'];
        $this->_ext = ($ext == '') ? $ext : '.' . $ext;
    }

    private function _assemblePattern($pattern) {

        if (!count($this->_variables)) {
            return;
        }

        foreach ($this->_variables as $param => $value) {
            $pattern = str_replace('{' . $param . '}', $value, $pattern);
        }
        $pattern = preg_replace("/\s*/", "", $pattern);
        preg_match_all("/\[(.*)\]/Uis", $pattern, $matches);
        foreach ($matches[1] as $str) {
            if (preg_match("/[{}]/", $str)) {
                throw new \Exception('Неверное использование шаблона ' . $str);
            }

            $result = eval("return $str;");
            if ($result) {
                $pattern = str_replace("[" . $str . "]", $result, $pattern);
            }
        }
        return $pattern;
    }

    private function _assembleFilename($filename, $mask, $miniature = NULL) {

        $this->_getPathInfo($filename);
        foreach ($this->_variables as $param => $value) {
            $mask = str_replace('{' . $param . '}', $value, $mask);
        }
        $mask = str_replace('<name>', $this->_name, $mask);
        $mask = str_replace('<extention>', $this->_ext, $mask);
        if (NULL != $miniature) {
            $mask = str_replace('<miniature>', $miniature, $mask);
        }
        $mask = preg_replace("/\s*/", "", $mask);
        preg_match_all("/\[(.*)\]/Uis", $mask, $matches);
        foreach ($matches[1] as $str) {
            eval("\$result = $str;");
            $mask = str_replace("[" . $str . "]", $result, $mask);
        }

        return $mask;
    }

    public function prepareStorage($destination = NULL) {

        try {

            if (NULL === $destination) {
                $destination = $this->_destination;
            }

            if (file_exists($destination)) {
                return TRUE;
            }

            $old = umask(0);
            $result = mkdir($destination, 0775, TRUE);
            umask($old);

            return $result;
        } catch (\Exception $e) {

            exit($e->getMessage());
        }
    }

    /**
     * Get destination path for files
     * @return string
     */
    public function getDestination() {

        return $this->_destination;
    }

    /**
     * Get the name of the uploaded file
     * @return string
     */
    public function getUploaded() {

        return $this->_uploaded;
    }

    public function getOriginal() {

        return $this->_original;
    }

    public function getPath($filename = null, $miniature = NULL) {

        if (!$filename && !$miniature) {
            return $this->_path;
        }

        if ($miniature) {
            $path = $this->getMiniaturePath($this->_path . "/" . $filename, $miniature);
        } else {
            $mask = "<name><extention>";
            if (array_key_exists('mask', $this->_asset)) {
                $mask = $this->_asset['mask'];
            }
            $target = $this->_assembleFilename($this->_path . "/" . $filename, $mask);
            $path = $this->_path . "/" . $target;
        }

        return $path;
    }

    public function getDummy($miniature = NULL) {

        $path = '';
        if (NULL === $miniature) {
            if (!array_key_exists('dummy', $this->_asset)) {
                return;
            }
            $path = $this->_asset['dummy'];
        }

        if (NULL != $miniature) {
            if (!array_key_exists('miniatures', $this->_asset)) {
                return;
            }
            if (!array_key_exists($miniature, $this->_asset['miniatures'])) {
                
            }
            if (!array_key_exists('dummy', $this->_asset['miniatures'][$miniature])) {
                return;
            }
            $path = $this->_asset['miniatures'][$miniature]['dummy'];
        }

        if (NULL !== $this->_theme) {
            $path = str_replace('{theme}', $this->_theme, $path);
        }

        return $path;
    }

    public function upload($miniature = NULL) {

        if (!$this->_file) {
            return FALSE;
        }

        $pathinfo = $this->mb_pathinfo($this->_file->getName());

        if (is_array($this->_file->getName())) {

            foreach ($pathinfo as $k => $p) {

                $this->_original[$k] = @$p['basename'];

                $name = @$p['filename'];
                if ($this->_asset['rename']) {
                    $name = md5(uniqid());
                }
                $this->_name[$k] = $name;
                $ext = @$p['extension'];
                $ext = ($ext == '') ? $ext : '.' . $ext;
                $this->_ext[$k] = $ext;

                $this->_uploaded[$k] = $name . $ext;

                if (!$this->prepareStorage()) {
                    return FALSE;
                }

                if (!is_writable($this->_destination)) {
                    return FALSE;
                }

                $filename = $this->_uploaded[$k];
                if (array_key_exists('mask', $this->_asset)) {
                    $filename = $this->_assembleFilename($filename, $this->_asset['mask']);
                }

                $destination = $this->_destination . "/" . $filename;
                if (!$this->_file->save($destination, $k)) {
                    throw new \Exception("Fail: upload to $destination");
                }

                chmod($destination, 0644);

                // RESIZE AND MAKE MINIATURES
                if ($this->isImage($destination)) {

                    $this->adapt($destination, $filename, $miniature);
                }
            }

            return TRUE;
        }

        $this->_original = @$pathinfo['basename'];

        $name = @$pathinfo['filename'];
        if ($this->_asset['rename']) {
            $name = md5(uniqid());
        }
        $this->_name = $name;
        $ext = @$pathinfo['extension'];
        $ext = ($ext == '') ? $ext : '.' . $ext;
        $this->_ext = $ext;

        $this->_uploaded = $name . $ext;

        if (!$this->prepareStorage()) {
            return FALSE;
        }

        if (!is_writable($this->_destination)) {
            return FALSE;
        }

        $filename = $this->_uploaded;
        if (array_key_exists('mask', $this->_asset)) {
            $filename = $this->_assembleFilename($filename, $this->_asset['mask']);
        }

        $destination = $this->_destination . "/" . $filename;
        if (!$this->_file->save($destination)) {
            throw new \Exception("Fail: upload to $destination");
        }
        chmod($destination, 0644);

        // RESIZE AND MAKE MINIATURES
        $this->adapt($destination, $filename, $miniature);


        return TRUE;
    }

    public function download($url) {

        $pathinfo = $this->mb_pathinfo($url);
      //   print_r($pathinfo);
       //  exit;
        $this->_original = @$pathinfo['basename'];

        $name = @$pathinfo['filename'];
        if ($this->_asset['rename']) {
            $name = md5(uniqid());
        }
        $this->_name = $name;
        $ext = @$pathinfo['extension'];
        $ext = ($ext == '') ? $ext : '.' . $ext;
        $this->_ext = $ext;

        $this->_uploaded = $name . $ext;

        if (!$this->prepareStorage()) {
            return FALSE;
        }

        if (!is_writable($this->_destination)) {
            return FALSE;
        }

        $filename = $this->_uploaded;
        if (array_key_exists('mask', $this->_asset)) {
            $filename = $this->_assembleFilename($filename, $this->_asset['mask']);
        }

        //$dest = "php/os/OpenServer/domains/yii-ad.local/advanced/backend/web";
        $destination = 
              //  $dest . 
                $this->_destination . "/" . $filename;
       // print_r($destination);
        if (!$this->_file->save($destination)) {
            throw new \Exception("Fail: upload to $destination");
        }
        chmod($destination, 0644);

        // RESIZE AND MAKE MINIATURES
        $this->adapt($destination, $filename, $miniature);


        return TRUE;
    }

    public function setSource($source) {

        $this->_source = $source;
    }

    public function getSource() {

        return $this->_source;
    }

    public function persist($source, $filename) {

        $source = $source . "/" . $filename;

        if (!file_exists($source)) {
            throw new \Exception("Fail: source $source not exists");
        }

        if (!$this->prepareStorage()) {
            throw new \Exception("Failed preparing storage $this->_destination");
        }

        if (!is_writable($this->_destination)) {
            throw new \Exception("Destination $this->_destination is not writable");
        }

        $target = $filename;
        if (array_key_exists('mask', $this->_asset)) {
            $target = $this->_assembleFilename($filename, $this->_asset['mask']);
        }
        $destination = $this->_destination . "/" . $target;

        if (!copy($source, $destination)) {
            throw new \Exception("Failed copy from $source to $destination");
        }

        @unlink($source);

        // RESIZE AND MAKE MINIATURES
        $this->adapt($destination, $filename);

        return TRUE;
    }

    public function adapt($source, $filename, $miniature = NULL) {

        // RESIZE
        $this->resize($source);

        // WATERMARK
        $this->watermark($source);

        // MINIATURES
        $this->buildMiniatures($source, $filename, $miniature);
    }

    public function resize($source) {

        if (!array_key_exists('resize', $this->_asset)) {
            return;
        }

        $image = new Image();
        $image->setSource($source)
                ->resize($this->_asset['resize']);
    }

    public function watermark($source) {

        if (!array_key_exists('watermark', $this->_asset)) {
            return;
        }

        $path = $this->_asset['watermark'];

        if (NULL !== $this->_theme) {
            $path = str_replace('{theme}', $this->_theme, $path);
        }

        $image = new Image();
        $image->setSource($source)->watermark($path);
    }

    public function buildMiniatures($source, $filename, $miniature) {

        if (!array_key_exists('miniatures', $this->_asset)) {
            return;
        }
        if (!file_exists($source)) {
            throw new \Exception("Fail: source $source not exists");
        }
        $image = new Image();

        $miniatures = (array) $this->_asset['miniatures'];

        if ($miniature && count($miniatures) && array_key_exists($miniature, $miniatures)) {
            $miniatures = array($miniature => $this->_asset['miniatures'][$miniature]);
        }

        if (!array_key_exists('documentroot', $this->_options)) {
            return;
        }

        foreach ($miniatures as $name => $options) {
            $destination = $this->getMiniaturePath($filename, $name);

            $destination = $this->_options['documentroot'] . "/" . $destination;

            if (!$this->prepareStorage(dirname($destination))) {
                throw new \Exception("Failed preparing storage $destination");
            }

            if (!is_writable(dirname($destination))) {
                throw new \Exception("Destination for miniature $destination is not writable");
            }

            if (!copy($source, $destination)) {
                throw new \Exception("Failed copy miniature from $source to $destination");
            }

            $image->setSource($destination);
            if (isset($options['crop_resize']) && $options['crop_resize']) {
                list($width, $height) = getimagesize($destination);
                $Ww = $width / $options['width'];
                $Hh = $height / $options['height'];
                if ($Ww > $Hh) {
                    $cropOptions = array(
                        'x' => (($width - ($options['width'] * $Hh)) / 2),
                        'y' => 0,
                        'height' => $height,
                        'width' => $options['width'] * $Hh,
                    );
                } else {
                    $cropOptions = array(
                        'x' => 0,
                        'y' => (($height - ($options['height'] * $Ww)) / 2),
                        'height' => $options['height'] * $Ww,
                        'width' => $width,
                    );
                }
                $image::cropResizeImage($destination, $cropOptions['x'], $cropOptions['y'], $cropOptions['width'], $cropOptions['height'], $options['width'], $options['height']);
            } else {
                $image->resize($options);
            }
        }
    }

    public function getMiniaturePath($filename, $miniature) {

        if (!array_key_exists('miniatures', $this->_asset)) {
            return;
        }
        $miniatures = $this->_asset['miniatures'];

        // PATTERN
        $pattern = "";
        if (array_key_exists('pattern', $miniatures[$miniature])) {
            $pattern = $this->_assemblePattern($miniatures[$miniature]['pattern']);
            $pattern .= "/";
        } else {
            if (array_key_exists('pattern', $this->_asset)) {
                $pattern = $this->_assemblePattern($this->_asset['pattern']);
                $pattern .= "/";
            }
        }

        // MASK
        $mask = "<name>_<miniature><extention>";
        if (array_key_exists('mask', $miniatures[$miniature])) {
            $mask = $miniatures[$miniature]['mask'];
        }
        $target = $this->_assembleFilename($filename, $mask, $miniature);
        $destination = $this->_asset['basepath'] . "/" . $pattern . $target;

        return $destination;
    }

    public function getFontPath() {

        if (!array_key_exists('fontPath', $this->_asset)) {
            return;
        }

        return $this->_asset['fontPath'];
    }

    public function deleteFile($fileName) {
        if (!$fileName)
            return FALSE;
        return unlink($this->getDestination() . '/' . $fileName);
    }

    public function update($userId, $oldFile, $newFile) {
        
    }

    public function unlink($userId, $filename) {
        
    }

    public function mb_pathinfo($filepath) {

        if (is_array($filepath)) {
            $ret = [];

            foreach ($filepath as $fp) {

                preg_match('%^(.*?)[\\\\/]*(([^/\\\\]*?)(\.([^\.\\\\/]+?)|))[\\\\/\.]*$%im', $fp, $m);

                $r = [];

                if ($m[1])
                    $r['dirname'] = $m[1];
                if ($m[2])
                    $r['basename'] = $m[2];
                if ($m[5])
                    $r['extension'] = $m[5];
                if ($m[3])
                    $r['filename'] = $m[3];

                $ret[] = $r;
            }

            return $ret;
        }

        preg_match('%^(.*?)[\\\\/]*(([^/\\\\]*?)(\.([^\.\\\\/]+?)|))[\\\\/\.]*$%im', $filepath, $m);
        if ($m[1])
            $ret['dirname'] = $m[1];
        if ($m[2])
            $ret['basename'] = $m[2];
        if ($m[5])
            $ret['extension'] = $m[5];
        if ($m[3])
            $ret['filename'] = $m[3];

        return $ret;
    }

    public function isImage($path, $simply = FALSE) {
        if ($simply) {

            $mb_pathinfo = $this->mb_pathinfo($path);

            if (in_array($mb_pathinfo['extension'], ['gif', 'jpeg', 'jpg', 'png', 'bmp', 'tiff'])) {
                return TRUE;
            }

            return FALSE;
        }

        if (!is_file($path) || !getimagesize($path)) {

            return FALSE;
        }

        return TRUE;
    }

}

<?php

namespace file\Manager;

interface FileInterface {

    public function upload();

    public function persist($source, $filename);

    public function update($userId, $oldFile, $newFile);

    public function unlink($userId, $filename);
}
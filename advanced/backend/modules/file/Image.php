<?php

namespace file;


class Image
{

    protected $_source;

    public function setSource($source)
    {

        $this->_source = $source;
        return $this;
    }

    public function getSource()
    {

        return $this->_source;
    }

    public function resize($options = array())
    {

        if (NULL == $this->_source)
            return;

        list($width, $height, $type) = getimagesize($this->_source);

        switch ($type) {
            case IMAGETYPE_JPEG :
                $image = @imagecreatefromjpeg($this->_source);
                break;
            case IMAGETYPE_PNG :
                $image = imagecreatefrompng($this->_source);
                break;
            case IMAGETYPE_GIF :
                $image = imagecreatefromgif($this->_source);
                break;
            default :
                return false;
        }

        $resized = array(
            'width'  => 0,
            'height' => 0
        );

        if (array_key_exists('max_width', $options) && array_key_exists('max_height', $options) &&
            ($width > $options['max_width'] || $height > $options['max_height'])
        ) {

            if ($width > $height) {
                $ratio  = $options['max_width'] / $width;
            } else {
                $ratio = $options['max_height'] /  $height;
            }
            $resized['width'] = intval($ratio * $width);
            $resized['height'] = intval($ratio * $height);
            $options['width'] = $resized['width'];
            $options['height'] = $resized['height'];

            $x = 0;
            $y = 0;

        } elseif(!array_key_exists('width', $options) && !array_key_exists('height', $options)) {
            return true;
        } else {
            if (!array_key_exists('width', $options))
                $options['width']  = 100;
            if (!array_key_exists('height', $options))
                $options['height'] = 100;

            if ($width > $height) {
                $resized['width']  = $options['width'];
                $resized['height'] = (int) (($height * $options['width']) / $width);
                if ($resized['height'] > $options['height']) {
                    $resized['width']  = (int) (($resized['width'] * $options['height']) / $resized['height']);
                    $resized['height'] = $options['height'];
                }
                $x = (int) (($options['width'] - $resized['width']) / 2);
                $y = (int) (($options['height'] - $resized['height']) / 2);
            } else {
                $resized['width']  = (int) ($width * $options['height'] / $height);
                $resized['height'] = $options['height'];
                $x                 = (int) (($options['width'] - $resized['width']) / 2);
                $y                 = 0;
            }
        }

        $thumb = imagecreatetruecolor($options['width'], $options['height']);
        $white = imagecolorallocate($thumb, 255, 255, 255);
        imagefilledrectangle($thumb, 0, 0, $options['width'], $options['height'], $white);
        imagecopyresampled($thumb, $image, $x, $y, 0, 0, $resized['width'], $resized['height'], $width, $height);

        imagedestroy($image);
        return imagejpeg($thumb, $this->_source, 100);
    }

    public function watermark($waterm, $where = 3)
    {

        $waterm = getcwd() . $waterm;

        $type = getimagesize($this->_source);

        switch ($type[2]) {
            case '1':
                $img  = imagecreatefromgif($this->_source);
                $func = 'imagegif';
                break;
            case '2':
                $img  = imagecreatefromjpeg($this->_source);
                $func = 'imagejpeg';
                break;
            case '3':
                $img  = imagecreatefrompng($this->_source);
                $func = 'imagepng';
                break;
            default:
                return false;
        }

        $size1 = getimagesize($this->_source);
        $size2 = getimagesize($waterm);

        $w = $size1[0] / 3;
        $k = $w / $size2[0];
        $h = $size2[1] * $k;

        $waterm = imagecreatefrompng($waterm);
        imagealphablending($img, true);
        // вычисляем, где "расположить" ватермарк
        switch ($where) {
            case 1:
                $position_w = 5;
                $position_h = 5;
                break;
            case 2:
                $position_w = $size1[0] - $w - 5;
                $position_h = 5;
                break;
            case 3:
                $position_w = $size1[0] - $size2[0] - 5;
                $position_h = $size1[1] - $size2[1] - 5;
                break;
            case 4:
                $position_w = 5;
                $position_h = $size1[1] - $h - 5;
                break;
        }
        imagecopyresampled($img, $waterm, $position_w, $position_h, 0, 0, $size2[0], $size2[1], $size2[0], $size2[1]);
        $func($img, $this->_source);
        imagedestroy($img);
        imagedestroy($waterm);
    }

    public static function cropImage($filename, $x, $y, $width, $height, $newName = NULL)
    {
        try {

            $quality = "";
            
            if (NULL == $newName) {
                $newName = $filename;
            }

            $imageInfo = @getimagesize($filename);
            $imageType = $imageInfo[2];

            $futureImage = @imagecreatetruecolor($width, $height);

            $baseImage   = "";
            $write_image = "";
            switch ($imageType) {
                case '1':
                    @imagecolortransparent($futureImage, @imagecolorallocate($futureImage, 0, 0, 0));
                    $baseImage   = @imagecreatefromgif($filename);
                    $write_image = 'imagegif';
                    $quality     = null;
                    break;
                case '2':
                    $baseImage   = @imagecreatefromjpeg($filename);
                    $write_image = 'imagejpeg';
                    $quality     = 100;
                    break;
                case '3':
                    @imagecolortransparent($futureImage, @imagecolorallocate($futureImage, 0, 0, 0));
                    @imagealphablending($futureImage, false);
                    @imagesavealpha($futureImage, true);
                    $baseImage   = @imagecreatefrompng($filename);
                    $write_image = 'imagepng';
                    $quality     = 0;
                    break;
                default:
                    $baseImage   = null;
            }

            @imagecopyresampled($futureImage, $baseImage, 0, 0, $x, $y, $width, $height, $width, $height);

            $write_image($futureImage, $filename, $quality);

            @imagedestroy($baseImage);
            @imagedestroy($futureImage);
        } catch (Exception $e) {
            return FALSE;
        }

        return TRUE;
    }


    public static function cropResizeImage($filename, $x, $y, $width, $height, $newWidth, $newHeight, $newName = null)
    {
        try {

            $quality = "";

            if (NULL == $newName) {
                $newName = $filename;
            }

            $imageInfo = @getimagesize($filename);
            $imageType = $imageInfo[2];

            $futureImage = @imagecreatetruecolor($newWidth, $newHeight);

            $baseImage   = "";
            $write_image = "";
            switch ($imageType) {
                case '1':
                    @imagecolortransparent($futureImage, @imagecolorallocate($futureImage, 0, 0, 0));
                    $baseImage   = @imagecreatefromgif($filename);
                    $write_image = 'imagegif';
                    $quality     = null;
                    break;
                case '2':
                    $baseImage   = @imagecreatefromjpeg($filename);
                    $write_image = 'imagejpeg';
                    $quality     = 100;
                    break;
                case '3':
                    @imagecolortransparent($futureImage, @imagecolorallocate($futureImage, 0, 0, 0));
                    @imagealphablending($futureImage, false);
                    @imagesavealpha($futureImage, true);
                    $baseImage   = @imagecreatefrompng($filename);
                    $write_image = 'imagepng';
                    $quality     = 0;
                    break;
                default:
                    $baseImage   = null;
            }

            @imagecopyresampled($futureImage, $baseImage, 0, 0, $x, $y, $newWidth, $newHeight, $width, $height);

            $write_image($futureImage, $filename, $quality);

            @imagedestroy($baseImage);
            @imagedestroy($futureImage);
        } catch (Exception $e) {
            return FALSE;
        }

        return TRUE;
    }

}

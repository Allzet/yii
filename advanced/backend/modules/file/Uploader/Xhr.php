<?php

namespace file\Uploader;

use file\Uploader\UploaderAbstract;

class Xhr extends UploaderAbstract {

    public function __construct($options = array()) {

        parent::__construct($options);
    }

    /**
     * Save the file to the specified path
     * @return boolean TRUE on success
     */
    public function save($path) {
        $input = fopen("php://input", "r");
        $temp = tmpfile();
        $realSize = stream_copy_to_stream($input, $temp);
        fclose($input);

        if ($realSize != $this->getSize()) {
            return false;
        }

        $target = fopen($path, "w");
        fseek($temp, 0, SEEK_SET);
        stream_copy_to_stream($temp, $target);
        fclose($target);

        return true;
    }

    /**
     * Get the original filename
     * @return string filename
     */
    public function getName() {

        return $_GET[$this->_name];
    }

    /**
     * Get the file size
     * @return integer file-size in byte
     */
    public function getSize() {

        if (isset($_SERVER["CONTENT_LENGTH"])) {
            return (int) $_SERVER["CONTENT_LENGTH"];
        } else {
            throw new \Exception('Getting content length is not supported.');
        }
    }

}

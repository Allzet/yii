<?php

namespace file\Uploader;

use file\Uploader\UploaderAbstract;

class Post extends UploaderAbstract {

    public function __construct($options = array()) {

        parent::__construct($options);
    }

    /**
     * Save the file to the specified path
     * @return boolean TRUE on success
     */
    public function save($path, $key = null) {

        if (is_array($_FILES[$this->_name]['tmp_name'])) {
            foreach ($_FILES[$this->_name]['tmp_name'] as $file => $name) {
                if (is_array($_FILES[$this->_name]['tmp_name'][$file])) {

                    return move_uploaded_file($_FILES[$this->_name]['tmp_name'][$file][$key], $path);
                }

                return move_uploaded_file($_FILES[$this->_name]['tmp_name'][$file], $path);
            }
        }

        return move_uploaded_file($_FILES[$this->_name]['tmp_name'], $path);
    }

    /**
     * Get the original filename
     * @return string filename
     */
    public function getName() {

        if (is_array($_FILES[$this->_name]['name'])) {
            foreach ($_FILES[$this->_name]['name'] as $file => $name) {
                return $name;
            }
        }

        return $_FILES[$this->_name]['name'];
    }

    /**
     * Get the file size
     * @return integer file-size in byte
     */
    public function getSize() {

        return $_FILES[$this->_name]['size'];
    }

}

<?php

namespace file\Uploader;

use file\Uploader\UploaderAbstract;

class Download extends UploaderAbstract {

    private $_options;

    public function __construct($options = array()) {

        // parent::__construct($options);
        if (!empty($options)) {
            $this->_options = $options;
        }
    }

    /**
     * Save the file to the specified path
     * @return boolean TRUE on success
     */
    public function save($path, $key = null) {

//print_r (\backend\models\News::$basepathloc);

        return file_put_contents($path, file_get_contents($this->_options['url']));
    }

    /**
     * Get the original filename
     * @return string filename
     */
    public function getName() {

        if (is_array($_FILES[$this->_name]['name'])) {
            foreach ($_FILES[$this->_name]['name'] as $file => $name) {
                return $name;
            }
        }

        return $_FILES[$this->_name]['name'];
    }

    /**
     * Get the file size
     * @return integer file-size in byte
     */
    public function getSize() {

        return $_FILES[$this->_name]['size'];
    }

}

<?php

namespace file\Uploader;

abstract class UploaderAbstract {

    const NAME = 'file';

    protected $_name;

    public function __construct($options = NULL) {

        $this->_name = self::NAME;

        if (is_array($options)) {
            if (array_key_exists('name', $options)) {
                $name = trim($options['name']);
                if (!empty($name)) {
                    $this->_name = $name;
                }
            }
        }
    }

}

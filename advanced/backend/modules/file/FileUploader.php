<?php

namespace backend;

use Yii;
use common\helpers\CommonHelper;

class FileUploader
{
    public $pictureFolders = '';
    public $thumbFolder = '';

    /**
     * Initialize uploader instance
     */
    public function __construct()
    {
        $this->pictureFolders = Yii::$app->params['pictureFolders'];
    }

    /*
     * Upload file to some folder
     */
    public function upload($model, $getFolder, $field = 'photo')
    {
        if(empty($this->pictureFolders)) return null;

        $modelName = CommonHelper::getClass($model);

        $folder = $getFolder($this->pictureFolders);

        $modelName = ucfirst($modelName);

        if(isset($_FILES[$modelName]['name'][$field]) && !empty($_FILES[$modelName]['name'][$field]))
        {
            $from = $_FILES[$modelName]['tmp_name'][$field];
            $fileName = $_FILES[$modelName]['name'][$field];
            $to = Yii::getAlias('@webroot').$folder.$fileName;

            move_uploaded_file($from, $to);

            $_POST[$modelName]['photo'] = $fileName; // fix POST variable
        } elseif(!empty($_POST))
            $_POST[$modelName]['photo'] = $model->$field; // fix POST variable
    }

}

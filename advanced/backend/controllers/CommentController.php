<?php

namespace backend\controllers;

use Yii;
use backend\models\Comment;
use backend\models\CommentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CommentController implements the CRUD actions for Comment model.
 */
class CommentController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    public function actionIndex() {

        if (Yii::$app->request->isAjax and Yii::$app->request->isPost) {
            $this->enableCsrfValidation = false;
            $post = Yii::$app->request->post();
            $notes = $post['notes'];
            foreach ($notes as $id => $text) {

                $model = Comment::findOne($id);
                $model->text = $text;
//                $x=$_SERVER['REMOTE_ADDR'];
//                echo $x;
//                exit;
                $model->save();
            }
            echo \yii\helpers\Json::encode([
                'output' => '',
                'message' => !empty($model->hasErrors()) ? 'Помилка' : '',
            ]);

            return;
        }

        if (empty($searchModel->id_news)) {
            $searchModel = new CommentSearch();
        } else {
            $searchModel = CommentSearch::findOne($model->id_news);
        }
        $model = new \backend\models\Comment();
        $model->ip = $_SERVER['REMOTE_ADDR'];
       
        $params = Yii::$app->request->queryParams;
        $searchModel->load($params);
        $comments = '';


        if (!empty($params['CommentSearch']['id_news'])) {

            \backend\models\Comment::$id_new = $params['CommentSearch']['id_news'];

            $writecom = \backend\models\CommentSearch::commentFromNews();

            $comments = $this->renderPartial('comments', [
                'dataProvider' => $writecom,
            ]);
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            $model->id_news = \backend\models\Comment::$id_new;
            $model->save();
//            return $this->redirect(['index', 'searchModel' => $searchModel]);
            return $this->refresh();
        } else {
            return $this->render('index', [
                        'comments' => $comments,
                        'searchModel' => $searchModel,
                        'id_news' => Comment::$id_new,
            ]);
        }
    }


    protected function findModel($id) {
        if (($model = Comment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetinfo() {


        if (!isset($_POST['comment_id']) || empty($_POST['comment_id']))
            return;
        $comment_id = $_POST['comment_id'];
        $model = Comment::findOne($comment_id);
        $model->status = $model->status == Comment::STATUS_DISABLED ? Comment::STATUS_ENABLED : Comment::STATUS_DISABLED;
        $model->save();

        return $model->status;
    }

}

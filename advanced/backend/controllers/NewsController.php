<?php

namespace backend\controllers;

use backend\models\News;
use backend\models\Newssearch;
use file\Manager\FileFactory;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class NewsController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new Newssearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate() {
        $model = new News();

        if ($model->load(Yii::$app->request->post()) and $model->save()) {

            $options = Yii::$app->params;
            if (!empty($_FILES['News']['name']['pict'][0])) {

                Yii::setAlias('@file', dirname(dirname(__DIR__)) . '/backend/modules/file');

                $fileManager = FileFactory::factory((object) $options['fileManager']);

                $fileManager->setAsset('news', ['created' => $model->crdate]);

                $fileManager->prepareStorage();

                $fileManager->prepareUploader(['name' => 'News']);

                $fileManager->upload();

                $model->url_pict = serialize($fileManager->getUploaded());
            } else {

                $model->url_pict = '';
            }
            $model->save();

            return $this->redirect(['view', 'id' => $model->id_news]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $options = Yii::$app->params;
            if (!empty($_FILES['News']['name']['pict'][0])) {

                Yii::setAlias('@file', dirname(dirname(__DIR__)) . '/backend/modules/file');

                $fileManager = FileFactory::factory((object) $options['fileManager']);

                $fileManager->setAsset('news', ['created' => $model->crdate]);

                $fileManager->prepareStorage();

                $fileManager->prepareUploader(['name' => 'News']);

                $fileManager->upload();

                $model->url_pict = serialize($fileManager->getUploaded());
            }
            $model->save();
            return $this->redirect(['view', 'id' => $model->id_news]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id) {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

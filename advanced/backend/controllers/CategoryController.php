<?php

namespace backend\controllers;

use Yii;
use backend\models\Category;
use backend\models\Categorysearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class CategoryController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex() {
        if (Yii::$app->request->isAjax and Yii::$app->request->isPost) {
            $this->enableCsrfValidation = false;
            $post = Yii::$app->request->post();
            $model = Category::findOne($post['editableKey']);
                        $model->categorycol = $post['Category'][$post['editableIndex']]['categorycol'];
                        $model->save();            
            
        echo \yii\helpers\Json::encode([
            'output' => '',
            'message' => !empty($model->hasErrors()) ? 'Помилка' : '',
        ]);
        return;
        }
        $model = new \backend\models\Category();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'model' => $model]);
        }
        $searchModel = new Categorysearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id) {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

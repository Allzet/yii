<?php

namespace backend\controllers;

use Yii;
use backend\models\Photogalery;
use backend\models\PhotogalerySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use file\Manager\FileFactory;

class PhotogaleryController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex() {
        $searchModel = new PhotogalerySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate() {
        $model = new Photogalery();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $options = Yii::$app->params;

            if (!empty($_FILES['Photogalery']['name']['picts'][0])) {

                Yii::setAlias('@file', dirname(dirname(__DIR__)) . '/backend/modules/file');

                $fileManager = FileFactory::factory((object) $options['fileManager']);

                $fileManager->setAsset('photogalery', ['created' => $model->date_create]);

                $fileManager->prepareStorage();

                $fileManager->prepareUploader(['name' => 'Photogalery']);

                $fileManager->upload();

                $model->arr_pictures = json_encode($fileManager->getUploaded());
                $arr = $fileManager->getUploaded();

                foreach ($arr as $k => &$v) {
                    $v = '';
                }
                $model->desc_pictures = json_encode($arr);
            } else {

                $model->arr_pictures = '';
                $model->desc_pictures = '';
            }

            $model->save();

            return $this->redirect(['update', 'id' => $model->id_album]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id) {

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $sort = Yii::$app->request->post('sort_list_2');


            $new_images = [];
            $new_desc = [];
            if (!empty($sort)) {
                $old_images = json_decode($model->arr_pictures);
                $old_desc = json_decode($model->arr_pictures);
                foreach (explode(',', $sort) as $key) {

                    $new_images[] = $old_images[$key];
                    $new_desc[] = $old_desc[$key];
                }
            } else {
                $new_images = json_decode($model->arr_pictures);
                $new_desc = json_decode($model->arr_pictures);
            }

            if (!empty($_FILES['Photogalery']['name']['picts'][0])) {
                $options = Yii::$app->params;
                Yii::setAlias('@file', dirname(dirname(__DIR__)) . '/backend/modules/file');

                $fileManager = FileFactory::factory((object) $options['fileManager']);

                $fileManager->setAsset('photogalery', ['created' => $model->date_create]);

                $fileManager->prepareStorage();

                $fileManager->prepareUploader(['name' => 'Photogalery']);

                $fileManager->upload();

                $qwerty = $fileManager->getUploaded();

                foreach ($qwerty as $k => $v) {
                    $new_images[] = $v;
                    $new_desc [] = '';
                }
            }
            if (!empty(Yii::$app->request->post('desc'))) {
                $new_desc = Yii::$app->request->post('desc');
            }
            $model->arr_pictures = json_encode((array) $new_images);
            $model->desc_pictures = json_encode((array) $new_desc);
            $model->save();
            return $this->redirect(['update', 'id' => $model->id_album]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteImg($id) {
        $model = $this->findModel($id);

        $imagess = (array) json_decode($model->arr_pictures);
        $descr = (array) json_decode($model->desc_pictures);

        unset($imagess[Yii::$app->request->get('imgid')]);
        unset($descr[Yii::$app->request->get('imgid')]);

        sort($imagess);
        sort($descr);

        $model->arr_pictures = !empty($imagess) ? json_encode($imagess) : '';
        $model->desc_pictures = !empty($descr) ? json_encode($descr) : '';

        $model->save();

        return $this->redirect(['update', 'id' => $id]);
    }

    protected function findModel($id) {
        if (($model = Photogalery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
